package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.RoleDto;
import com.zzyl.vo.RoleVo;

import java.util.List;
import java.util.Set;

public interface RoleService {

    /**
     * 角色分页条件查询
     *
     * @param pageNum
     * @param pageSize
     * @param dto
     * @return
     */
    PageBean<RoleVo> pageQuery(Integer pageNum, Integer pageSize, RoleDto dto);

    /**
     * 添加角色
     *
     * @param dto
     */
    void addRole(RoleDto dto);

    /**
     * 修改角色（启用禁用、编辑、角色授权）
     *
     * @param dto
     */
    void updateRole(RoleDto dto);

    /**
     * 根据角色查询对应的资源编号
     *
     * @param roleId
     * @return
     */
    Set<String> findCheckedResource(Long roleId);

    /**
     * 根据id删除
     *
     * @param id
     */
    void deleteById(Long id);

    /**
     * 查询所有角色、角色下拉框
     */
    List<RoleVo> list();
}
