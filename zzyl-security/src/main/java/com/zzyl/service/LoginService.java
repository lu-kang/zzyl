package com.zzyl.service;

import com.zzyl.dto.LoginDto;
import com.zzyl.vo.LoginVo;

public interface LoginService {

    /**
     * 后端用户登录
     *
     * @param dto
     * @return
     */
    LoginVo login(LoginDto dto);
}
