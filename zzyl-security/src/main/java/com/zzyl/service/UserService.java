package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.UserDto;
import com.zzyl.vo.UserVo;

import java.util.List;

/**
 * 用户表服务类
 */
public interface UserService {

    /**
     * 分页条件查询
     *
     * @param pageNum
     * @param pageSize
     * @param dto
     * @return
     */
    PageBean<UserVo> pageQuery(Integer pageNum, Integer pageSize, UserDto dto);

    /**
     * 创建用户表
     *
     * @param userDto 对象信息
     * @return User
     */
    void createUser(UserDto userDto);

    /**
     * 修改用户表
     *
     * @param userDto 对象信息
     * @return Boolean
     */
    void updateUser(UserDto userDto);

    /**
     * 根据用户id集合删除用户
     *
     * @param userId
     * @return
     */
    void deleteUserById(Long userId);


    /**
     * 启用或禁用用户
     *
     * @param id
     * @param status
     */
    void isEnable(Long id, String status);


    /**
     * 多条件查询用户表列表
     *
     * @param userDto 查询条件
     * @return: List<User>
     */
    List<UserVo> findUserList(UserDto userDto);

    /**
     * 重置密码
     *
     * @param userId
     * @return
     */
    void resetPasswords(Long userId);

    /**
     * 获取个人信息
     *
     * @return
     */
    UserVo getCurrentUser();
}
