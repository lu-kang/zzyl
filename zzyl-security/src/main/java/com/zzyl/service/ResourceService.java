package com.zzyl.service;

import com.zzyl.dto.ResourceDto;
import com.zzyl.vo.MenuVo;
import com.zzyl.vo.ResourceVo;
import com.zzyl.vo.TreeVo;

import java.util.List;

public interface ResourceService {

    /**
     * 根据用户id查询对应的资源数据
     *
     * @return
     */
    List<MenuVo> getMyMenus();

    /**
     * 资源列表
     *
     * @param dto
     * @return
     */
    List<ResourceVo> listResource(ResourceDto dto);

    /**
     * 资源树形结构
     *
     * @return
     */
    TreeVo resourceTreeVo();

    /**
     * 添加资源
     *
     * @param dto
     */
    void addResource(ResourceDto dto);

    /**
     * 修改资源
     *
     * @param resourceDto
     */
    void updateResource(ResourceDto resourceDto);

    /**
     * 启用禁用
     *
     * @param resourceDto
     * @return
     */
    void isEnable(ResourceDto resourceDto);

    /**
     * 删除菜单
     *
     * @param resourceNo
     */
    void deleteByResourceNo(String resourceNo);

}
