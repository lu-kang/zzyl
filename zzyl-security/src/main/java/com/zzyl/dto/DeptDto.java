package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@ApiModel("部门DTO")
public class DeptDto extends BaseDto {

    private String parentDeptNo; // 父部门编号
    private String deptNo; // 部门编号
    private String deptName; // 部门名称
    private Integer sortNo; // 排序
    private Long leaderId; // 负责人Id
    private String dataState; // 是否启用（0:启用，1:禁用）
    private Integer level = 4; // 层级

}
