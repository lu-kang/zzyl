package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@ApiModel("角色DTO")
public class RoleDto extends BaseDto {

    private String roleName; // 角色名称
    private String label; // 角色标识
    private Integer sortNo; // 排序
    private String remark; // 备注
    private String userId; // 人员查询部门：当前人员Id
    private String dataScope; // 数据范围（0自定义  1本人 2本部门及以下 3本部门 4全部）
    private String dataState; // 是否启用（0:启用，1:禁用）
    private String[] checkedResourceNos; // TREE结构：选中资源No
    private String[] checkedDeptNos; // TREE结构：选中部门No
}
