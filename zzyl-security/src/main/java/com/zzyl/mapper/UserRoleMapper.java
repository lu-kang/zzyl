package com.zzyl.mapper;

import com.zzyl.entity.UserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserRoleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(UserRole record);

    int insertSelective(UserRole record);

    UserRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserRole record);

    int updateByPrimaryKey(UserRole record);

    int batchInsert(@Param("list") List<UserRole> list);

    @Delete("delete from sys_user_role where user_id = #{userId}")
    boolean deleteUserRoleByUserId(Long id);

    @Select("select count(1) from sys_user_role where role_id = #{roleId}")
    int countUserRoleByRoleId(Long roleId);

    @Select("select sur.user_id from sys_user_role sur left join sys_role sr on sur.role_id = sr.id where sr.role_name = #{roleName}")
    List<Long> selectUserIdsByRoleName(String roleName);
}