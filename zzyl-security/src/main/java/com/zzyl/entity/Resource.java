package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Resource extends BaseEntity {

    private String dataState;//数据状态（0正常 1停用）
    private String icon;//图标
    private String label;//权限标识
    private String parentResourceNo;//父资源编号
    private String requestPath;//请求地址
    private String resourceName;//资源名称
    private String resourceNo;//资源编号
    private String resourceType;//资源类型：s平台 c目录 m菜单 r按钮
    private Integer sortNo;//排序
}
