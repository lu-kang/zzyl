package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Role extends BaseEntity {

    private String dataScope;//数据范围：0自定义  1本人 2本部门及以下 3本部门 4全部
    private String dataState;//数据状态（0正常 1停用）
    private String label;//权限标识
    private String roleName;//角色名称
    private Integer sortNo;//排序
}
