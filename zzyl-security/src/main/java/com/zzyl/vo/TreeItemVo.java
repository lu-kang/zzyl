package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@ApiModel("资源树结构体 菜单项VO")
@NoArgsConstructor
@AllArgsConstructor
public class TreeItemVo implements Serializable {

    private String id; // 节点ID
    private String label; // 显示内容
    private List<TreeItemVo> children = new ArrayList<>(); // 子节点
}
