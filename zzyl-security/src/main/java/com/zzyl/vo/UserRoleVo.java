package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("用户角色VO")
public class UserRoleVo {

    private String userName;//用户真名
    private String roleName;//角色名称
    private Long id;//用户id
}
