package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@ApiModel("用户添加VO")
public class UserAddVo extends BaseVo {

    private String username; // 用户账号
    private String password; // 密码
    private String email; // 用户邮箱
    private String realName; // 真实姓名
    private String mobile; // 手机号码
    private String sex; // 用户性别（0男 1女 2未知）
    private Set<String> roleVoIds; // 查询用户：用户角色Ids
    private String deptNo; // 部门编号【当前】
    private String postNo; // 职位编号【当前】
}
