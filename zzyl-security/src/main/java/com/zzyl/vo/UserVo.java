package com.zzyl.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zzyl.base.BaseVo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * 用户表
 */
@Data
@NoArgsConstructor
public class UserVo extends BaseVo {

    private String username; // 用户账号
    @JsonIgnore
    private String password; // 密码
    private String userType; // 用户类型（0:系统用户,1:客户）
    private String nickName; // 用户昵称

    private String postNo; // 职位编号
    private String deptNo; // 部门编号
    private String postName; // 职位名称
    private String deptName; // 部门名称

    private String email; // 用户邮箱
    private String realName; // 真实姓名
    private String mobile; // 手机号码
    private String sex; // 用户性别（0男 1女 2未知）
    private String remark; // 备注

    private Set<String> roleVoIds; // 查询用户：用户角色Ids
    private Set<String> roleLabels; // 查询用户：用户角色标识

    private Set<String> roleNames; // 查询用户：用户角色名称
}
