package com.zzyl.job;

import com.zzyl.mapper.DeviceDataMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 设备上报数据清理任务
 */
@Component
@Slf4j
public class DeviceDataClearJob {


    @Autowired
    private DeviceDataMapper deviceDataMapper;

    /**
     * 每天凌晨1点清理30天之前的数据
     */
    //@Scheduled(cron = "0 0 1 * * ?")
    public void clearDeviceDataJob() {
        log.info("设备上报数据,定时清理开始....");
        deviceDataMapper.clearDeviceDataJob();
        log.info("设备上报数据,定时清理结束....");
    }
}
