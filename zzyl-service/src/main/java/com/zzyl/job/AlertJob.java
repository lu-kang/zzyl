package com.zzyl.job;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.zzyl.constant.CacheConstants;
import com.zzyl.dto.AlertNotifyDto;
import com.zzyl.entity.AlertData;
import com.zzyl.entity.AlertRule;
import com.zzyl.mapper.AlertDataMapper;
import com.zzyl.mapper.AlertRuleMapper;
import com.zzyl.mapper.DeviceMapper;
import com.zzyl.mapper.UserRoleMapper;
import com.zzyl.vo.DeviceDataVo;
import com.zzyl.ws.WebSocketServer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 设备上报数据报警规则过滤
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class AlertJob {

    final AlertRuleMapper alertRuleMapper;
    final StringRedisTemplate stringRedisTemplate;
    final DeviceMapper deviceMapper;
    final UserRoleMapper userRoleMapper;
    final AlertDataMapper alertDataMapper;
    final WebSocketServer webSocketServer;

    /**
     * 第一层逻辑：
     * 1）查询所有的报警规则，是否有生效的规则，如果没有直接结束任务
     * 2）查询所有的Redis上报的最新数据，判断是否为空，如果为空直接结束任务
     * 3）将小集合List<DeviceDataVo>汇聚成大集合List<DeviceDataVo>
     * 4）遍历大集合，处理每一个设备的属性值（诊断属性值）
     */
    @Scheduled(cron = "0/10 * * * * ?")
    public void deviceDataAlertFilter() {
        log.info("报警数据诊断执行");
        //1）查询所有的报警规则，是否有生效的规则，如果没有直接结束任务
        List<AlertRule> ruleList = alertRuleMapper.selectAll();
        if (CollUtil.isEmpty(ruleList)) {
            return;
        }

        //2）查询所有的Redis上报的最新数据，判断是否为空，如果为空直接结束任务
        List<Object> values = stringRedisTemplate.opsForHash().values(CacheConstants.IOT_DEVICE_LAST_DATA);
        if (CollUtil.isEmpty(values)) {
            return;
        }

        //3）将小集合List<DeviceDataVo>汇聚成大集合List<DeviceDataVo>
        List<DeviceDataVo> allDeviceDataList = new ArrayList<>();
        for (Object json : values) {
            //将json字符串转为List对象
            List<DeviceDataVo> deviceDataVos = JSON.parseArray(json.toString(), DeviceDataVo.class);
            allDeviceDataList.addAll(deviceDataVos);
        }

        //4）遍历大集合，处理每一个设备的属性值（诊断属性值）
        for (DeviceDataVo deviceDataVo : allDeviceDataList) {
            alertFilter(deviceDataVo);
        }
    }


    /**
     * 第二层逻辑：DeviceDataVo
     * 1）校验设备数据的上报时间，是否超过1分钟，超过直接结束任务
     * 2）根据设备属性查询对应的匹配规则（productKey=xxx  and  iot=-1 or xxx  and  functionId=xxx）
     * 3）判断规则是否有为空，如果为空，直接结束任务
     * 4）遍历规则集合，使用每一个规则来校验设备数据（AlertRule、DeviceDataVo）
     */
    public void alertFilter(DeviceDataVo deviceDataVo) {
        //1）校验设备数据的上报时间，是否超过1分钟，超过直接结束任务
        LocalDateTime alarmTime = deviceDataVo.getAlarmTime();//设备上报时间
        LocalDateTime now = LocalDateTime.now();//当前时间
        //计算两个时间的差距
        Duration duration = Duration.between(alarmTime, now);
        if (duration.getSeconds() > 60) {
            //说明数据上报的时间太久了，舍弃掉
            return;
        }

        //2）根据设备属性查询对应的匹配规则
        List<AlertRule> ruleList = alertRuleMapper.selectByFunctionId(deviceDataVo.getProductKey(),
                deviceDataVo.getIotId(),
                deviceDataVo.getFunctionId());

        //3）判断规则是否有为空，如果为空，直接结束任务
        if (CollUtil.isEmpty(ruleList)) {
            return;
        }

        //4）遍历规则集合，使用每一个规则来校验设备数据（AlertRule、DeviceDataVo）
        for (AlertRule alertRule : ruleList) {
            deviceDateAlarmHandler(alertRule, deviceDataVo);
        }
    }


    /**
     * 第三层逻辑
     * 1）校验规则生效时间
     * 2）校验规则阈值
     * 3）校验沉默周期
     * 4）校验持续周期
     */
    public void deviceDateAlarmHandler(AlertRule alertRule, DeviceDataVo deviceDataVo) {
        //1）校验规则生效时间
        LocalTime startTime = LocalTime.parse(alertRule.getAlertEffectivePeriod().split("~")[0]);
        LocalTime endTime = LocalTime.parse(alertRule.getAlertEffectivePeriod().split("~")[1]);

        //获取数据上报时间
        LocalTime alarmTime = deviceDataVo.getAlarmTime().toLocalTime();
        if (alarmTime.isBefore(startTime) || alarmTime.isAfter(endTime)) {
            return;
        }

        //2）校验规则阈值
        Double dataValue = Double.valueOf(deviceDataVo.getDataValue()); //设备的数据
        Double ruleValue = Double.valueOf(alertRule.getValue()); //规则的数据

        int compare = NumberUtil.compare(dataValue, ruleValue); //比较两个数据的大小

        //设备数据大于规则的数据，并且操作符=“>=”
        //设备数据小于规则的数据，并且操作符=“<”
        if (compare >= 0 && alertRule.getOperator().equals(">=")
                || compare < 0 && alertRule.getOperator().equals("<")) {
            log.info("设备的数据达到规则阈值:{} {}", alertRule, deviceDataVo);
        } else {
            //TODO 删除redis的 alertTriggerCountRedisKey
            String alertTriggerCountRedisKey = CacheConstants.ALERT_TRIGGER_COUNT + deviceDataVo.getIotId() + ":" + alertRule.getId(); //iotId、ruleId
            stringRedisTemplate.delete(alertTriggerCountRedisKey);
            return;
        }

        //3）校验沉默周期
        String alertSilentCycleRedisKey = CacheConstants.ALERT_SILENT_CYCLE + deviceDataVo.getIotId() + ":" + alertRule.getId(); //iotId、ruleId
        Boolean hasKey = stringRedisTemplate.hasKey(alertSilentCycleRedisKey);
        if (hasKey) {
            return;
        }

        //4）校验持续周期
        String alertTriggerCountRedisKey = CacheConstants.ALERT_TRIGGER_COUNT + deviceDataVo.getIotId() + ":" + alertRule.getId(); //iotId、ruleId
        /*String count = stringRedisTemplate.opsForValue().get(alertTriggerCountRedisKey);
        count = StrUtil.nullToDefault(count, "0"); //将null转为默认字符串0
        //将次数累加1
        Integer triggerCount = Integer.parseInt(count) + 1;
        stringRedisTemplate.opsForValue().set(alertTriggerCountRedisKey, triggerCount.toString());*/
        Long triggerCount = stringRedisTemplate.opsForValue().increment(alertTriggerCountRedisKey, 1); //将redis数据累计加1
        //和规则的持续周期比较
        if (triggerCount < alertRule.getDuration()) {
            return;
        }

        //////////////////////////////////////////////////// 报警了
        //////////////////////////////////////////////////// 报警了
        /**
         * 1）添加沉默周期
         * 2）删除持续周期
         * 3）查询通知的人员（护理员|维修工 + 超级管理员）
         * 4）添加报警数据
         * 5）TODO 发送WebSocket消息
         */
        //1）添加沉默周期
        stringRedisTemplate.opsForValue().set(alertSilentCycleRedisKey, "true", alertRule.getAlertSilentPeriod(), TimeUnit.MINUTES);
        //2）删除持续周期
        stringRedisTemplate.delete(alertTriggerCountRedisKey);

        //3）查询通知的人员（护理员|维修工 + 超级管理员）   //0楼层 1房间 2床位 -1老人
        List<Long> userIds = new ArrayList<>();
        if (deviceDataVo.getPhysicalLocationType() == -1) { //护理员
            userIds = deviceMapper.selectNursingIdsByIotIdWithElder(deviceDataVo.getIotId());
        }
        if (deviceDataVo.getPhysicalLocationType() == 2) {  //护理员
            userIds = deviceMapper.selectNursingIdsByIotIdWithBed(deviceDataVo.getIotId());
        }
        if (deviceDataVo.getPhysicalLocationType() == 1) {  //维修工
            userIds = userRoleMapper.selectUserIdsByRoleName("维修工");
        }

        //通知超级管理员
        List<Long> adminUserIds = userRoleMapper.selectUserIdsByRoleName("超级管理员");

        //最终通知的使用人的用户id
        List<Long> allUserIds = CollUtil.addAllIfNotContains(userIds, adminUserIds);

        //4）添加报警数据
        batchInsertAlertData(allUserIds, alertRule, deviceDataVo);

        //5）TODO 发送WebSocket消息（发给指定的人）
        AlertNotifyDto alertNotifyDto = new AlertNotifyDto();
        BeanUtil.copyProperties(alertRule, alertNotifyDto);
        BeanUtil.copyProperties(deviceDataVo, alertNotifyDto);
        alertNotifyDto.setVoiceNotifyStatus(1);//语音通知状态，0：关闭，1：开启
        alertNotifyDto.setNotifyType(1);//报警通知类型，0：解除报警，1：报警
        alertNotifyDto.setIsAllConsumer(false);//是否全员通知
        webSocketServer.sendMessageToConsumer(alertNotifyDto, allUserIds);
    }

    /**
     * 批量添加报警数据
     *
     * @param allUserIds
     * @param alertRule
     * @param deviceDataVo
     */
    private void batchInsertAlertData(List<Long> allUserIds, AlertRule alertRule, DeviceDataVo deviceDataVo) {

        String alertReason = StrUtil.format("{} {} {}, 持续 {} 个周期就报警",
                alertRule.getFunctionName(),
                alertRule.getOperator(),
                alertRule.getValue(),
                alertRule.getDuration());

        AlertData alertData = new AlertData();
        //将AlertRule的信息封装到AlertData中
        BeanUtil.copyProperties(alertRule, alertData, "createTime", "updateTime", "createBy", "updateBy");
        //将DeviceDataVo的信息封装到AlertData中
        BeanUtil.copyProperties(deviceDataVo, alertData, "createTime", "updateTime", "createBy", "updateBy");
        alertData.setStatus(0);//待处理
        alertData.setAlertReason(alertReason);//报警原因
        alertData.setAlertRuleId(alertRule.getId());//报警规则id
        alertData.setType(alertRule.getAlertDataType());//报警数据类型

        //map ->  mapping  A->B
        List<AlertData> collect = allUserIds.stream().map(userId -> {
            //将userId的信息封装到AlertData中
            alertData.setUserId(userId);
            return BeanUtil.toBean(alertData, AlertData.class);
        }).collect(Collectors.toList());

        alertDataMapper.batchInsert(collect);
    }

}