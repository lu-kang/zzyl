package com.zzyl.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 账单超时任务
 */
@Slf4j
@Component
public class OrderJob {

    /**
     * 定时任务查询所有订单 判断下单时间过了15分钟还未付款 改为已关闭
     * 过了3个月 改成已完成
     */
    //@Scheduled(cron = "0 * * * * ?")
    public void orderJob() {
    }
}
