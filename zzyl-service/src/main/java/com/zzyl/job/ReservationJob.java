package com.zzyl.job;

import com.zzyl.mapper.ReservationMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 预约过期任务
 */
@Component
@Slf4j
public class ReservationJob {

    @Autowired
    ReservationMapper reservationMapper;

    @Scheduled(cron = "0 0/30 8-18 * * ?")
    public void checkReservationTimeout() {
        log.info("检查预约超时定时任务执行:{}", LocalDateTime.now());
        //1. 查询预约表reservation
        //update reservation set status=3, remark='预约超时，系统自动取消', update_time=? where time <= ? and status=0
    }

}
