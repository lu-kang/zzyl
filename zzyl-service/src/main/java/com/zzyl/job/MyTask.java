package com.zzyl.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@Component
public class MyTask {

    /**
     * 定时任务
     * 1. 按照固定频率执行（每隔 10秒钟|半个小时|。。。执行）
     * 2. 指定一个时间执行（每天凌晨1点）
     */
    //@Scheduled(fixedRate = 10, timeUnit = TimeUnit.SECONDS)
    //@Scheduled(cron = "0 0/30 * * * ?")
    //@Scheduled(cron = "0/5 * * * * ?")
    //@Scheduled(fixedRate = 10, timeUnit = TimeUnit.SECONDS)

    @Scheduled(cron = "0 0/30 8-18 * * ?")
    public void task() {
        System.out.println(LocalDateTime.now());
    }
}
