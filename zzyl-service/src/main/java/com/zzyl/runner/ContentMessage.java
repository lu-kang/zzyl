package com.zzyl.runner;

import lombok.Data;

import java.util.Map;

@Data
public class ContentMessage {

    public String iotId; //设备唯一标号
    public String deviceName; //设备名称
    public String productKey; //产品key
    public Map<String, ItemData> items; //上报的数据

    @Data
    public static class ItemData {
        public String value;
        public Long time;
    }
}
