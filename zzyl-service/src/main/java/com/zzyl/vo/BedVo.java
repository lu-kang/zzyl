package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel("床位实体类")
public class BedVo extends BaseVo {

    private String bedNumber; // 床位编号
    private Integer bedStatus; // 床位状态: 未入住0, 已入住1 入住申请中2
    private Long roomId; // 房间ID
    private Integer sort; // 排序号
    private List<String> nursingNames; // 护理员名称
    private List<Long> nursingIds; // 护理员ID
    private List<UserVo> userVos; // 护理员
    private String name; // 老人姓名
    private String floorName; // 楼层名称
    private Long floorId; // 楼层ID
    private String typeName; // 房间类型名称
    private String code; // 房间编号
    private BigDecimal price; // 床位费用
    private Long elderId; // 老人ID
    private Integer status = 0; // 状态
    private String lname; // 等级名称

    private CheckInConfigVo checkInConfigVo; // 入住配置
    private List<DeviceVo> deviceVos; // 设备列表
}
