package com.zzyl.vo;

import lombok.Data;

@Data
public class QueryThingModelVo {
    public String thingModelJson;
}
