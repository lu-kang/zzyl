package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "NursingProjectPlanVo对象", description = "护理项目计划VO")
public class NursingProjectPlanVo extends BaseVo {

    private Long planId; // 计划id
    private Long projectId; // 项目id
    private String executeTime; // 计划执行时间
    private Integer executeCycle; // 执行周期  0 天 1 周 2月
    private Integer executeFrequency; // 执行频次
    private String projectName; // 项目名称
}
