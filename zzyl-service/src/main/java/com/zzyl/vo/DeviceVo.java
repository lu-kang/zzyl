package com.zzyl.vo;

import com.aliyun.tea.NameInMap;
import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("设备信息响应模型")
public class DeviceVo extends BaseVo {

    private Integer locationType; // 位置类型 0 随身设备 1固定设备
    private Long bindingLocation; // 绑定位置
    private String deviceName; // 设备名称
    private Integer physicalLocationType; // 物理位置类型
    private String iotId; // 物联网设备ID
    private String deviceDescription; // 位置名称回显字段
    private Integer haveEntranceGuard; // 产品是否包含门禁，0：否，1：是

    @NameInMap("DeviceSecret")
    public String deviceSecret; // 设备密钥
    @NameInMap("FirmwareVersion")
    public String firmwareVersion; // 固件版本
    @NameInMap("GmtActive")
    public String gmtActive; // 激活时间
    @NameInMap("GmtCreate")
    public String gmtCreate; // 创建时间
    @NameInMap("GmtOnline")
    public String gmtOnline; // 上线时间
    @NameInMap("IpAddress")
    public String ipAddress; // IP地址
    @NameInMap("Nickname")
    public String nickname; // 设备备注名称
    @NameInMap("NodeType")
    public Integer nodeType; // 节点类型
    @NameInMap("Owner")
    public Boolean owner; // 设备所有者
    @NameInMap("ProductKey")
    public String productKey; // 产品key
    @NameInMap("ProductName")
    public String productName; // 产品名称
    @NameInMap("Region")
    public String region; // 地区
    @NameInMap("Status")
    public String status; // 设备状态
    @NameInMap("UtcActive")
    public String utcActive; // 激活时间
    @NameInMap("UtcCreate")
    public String utcCreate; // 创建时间
    @NameInMap("UtcOnline")
    public String utcOnline; // 上线时间

    private List<DeviceDataVo> deviceDataVos; // 设备数据集合
}
