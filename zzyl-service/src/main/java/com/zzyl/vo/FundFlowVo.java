package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(description = "资金流水信息")
public class FundFlowVo extends BaseVo {

    private Integer balanceType; // 余额类型
    private Integer fundDirection; // 资金流向
    private String relatedBillNo; // 关联单据号
    private String flowReason; // 资金流水原因
    private BigDecimal amount; // 金额
    private Long elderId; // 老人ID
    private String elderName; // 老人姓名
    private String bedNo; // 床位号
}
