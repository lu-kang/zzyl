package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("消息响应模型")
public class MessageVo {

    private Long id; // 消息id
    private String title; // 标题
    private String content; // 消息内容
    private Integer type; // 消息类型，0：报警通知
    private Long userId; // 消息接收人id，特别的，后台：-1
    private Integer isRead; // 是否已读，0：未读，1：已读
    private LocalDateTime readTime; // 已读时间
    private Long relevantId; // 关联id,如果是报警通知，则关联报警数据id
    private LocalDateTime createTime; // 创建时间
}
