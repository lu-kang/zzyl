package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(value = "登录对象")
public class WxLoginVo {

    private String token; // JWT token
    private String nickName; // 昵称
}
