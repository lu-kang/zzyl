package com.zzyl.vo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TimeCountVo {

    private LocalDateTime time;//时间
    private Integer count;//次数
}
