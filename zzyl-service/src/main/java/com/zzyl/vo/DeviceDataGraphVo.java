package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 设备数据曲线图响应模型
 **/
@Data
@ApiModel("设备数据曲线图响应模型")
public class DeviceDataGraphVo {

    private String dateTime; // 日期
    private Double dataValue; // 数据
}
