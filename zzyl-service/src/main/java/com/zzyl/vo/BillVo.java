package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(description = "账单信息")
public class BillVo extends BaseVo {

    private String billNo; // 账单编号
    private Integer billType; // 账单类型（0：月度账单，1服务账单）
    private String billMonth; // 账单月份
    private Long elderId; // 老人ID
    private BigDecimal billAmount; // 账单金额
    private BigDecimal otherAmount; // 其他支付方式的金额
    private BigDecimal payableAmount; // 应付金额
    private LocalDateTime paymentDeadline; // 缴费截止日期
    private Integer transactionStatus; // 交易状态 账单状态（0：未支付，1已支付, 2已关闭）
    private LocalDateTime billStartTime; // 账单开始时间
    private LocalDateTime billEndTime; // 账单结束时间
    private Integer totalDays; // 账单总天数
    private BigDecimal currentCost; // 本期应付
    private BigDecimal depositAmount; // 押金金额
    private BigDecimal prepaidAmount; // 预付款支付金额
    private String lname; // 等级名称
    private String typeName; // 房间类型名称
    private ElderVo elderVo; // 老人信息
    private CheckInConfigVo checkInConfigVo; // 入住配置
    private Long tradingOrderNo; // 交易号
    private String orderNo; // 订单编号
    private Long orderId; // 订单ID
    private List<TradingVo> tradingVo; // 支付记录
    private RefundRecordVo refundRecordVo; // 退款记录
    private BigDecimal total; // 总欠费金额
    private BedVo bedVo; // 床位信息
    private String memberCreator;
}
