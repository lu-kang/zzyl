package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "BalanceDto", description = "余额信息")
public class BalanceVo extends BaseVo {

    private BigDecimal prepaidBalance; // 预付款余额
    private BigDecimal depositAmount; // 押金金额
    private BigDecimal arrearsAmount; // 欠费金额
    private LocalDateTime paymentDeadline; // 缴费截止日期
    private Integer status; // 状态
    private Long elderId; // 老人ID
    private String elderName; // 老人姓名
    private String bedNo; // 床位号
    private String description; // 扣款备注
}
