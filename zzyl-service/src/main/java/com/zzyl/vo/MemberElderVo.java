package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("客户老人关联实体类")
public class MemberElderVo extends BaseVo {

    private Long memberId; // 客户id
    private Long elderId; // 老人id
    private ElderVo elderVo; // 老人信息
    private BedVo bedVo; // 床位信息
    private RoomVo roomVo; // 房间信息
    private List<DeviceVo> deviceVos; // 老人关联的设备列表
}


