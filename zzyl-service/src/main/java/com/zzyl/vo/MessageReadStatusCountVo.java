package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("消息已读未读状态统计数量响应模型")
public class MessageReadStatusCountVo {

    private Long unReadCount; // 未读消息数量
    private Long completedReadCount; // 已读消息数量

    /**
     * 实例化一个消息已读未读状态统计数量响应模型
     */
    public static MessageReadStatusCountVo instance() {
        MessageReadStatusCountVo messageReadStatusCountVo = new MessageReadStatusCountVo();
        messageReadStatusCountVo.setUnReadCount(0L);
        messageReadStatusCountVo.setCompletedReadCount(0L);
        return messageReadStatusCountVo;
    }
}
