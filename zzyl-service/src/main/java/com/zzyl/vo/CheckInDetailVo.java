package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "入住详情响应模型")
public class CheckInDetailVo {

    private CheckInElderVo checkInElderVo; // 老人响应信息
    private List<ElderFamilyVo> elderFamilyVoList; // 家属响应信息
    private CheckInConfigVo checkInConfigVo; // 入住配置响应信息
    private CheckInContractVo checkInContractVo; // 签约办理响应信息
}
