package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(description = "预付费充值记录")
public class PrepaidRechargeRecordVo extends BaseVo {

    private BigDecimal rechargeAmount; // 充值金额
    private String rechargeVoucher; // 充值凭证
    private String rechargeMethod; // 充值方式
    private Long elderId; // 老人ID
    private String elderName; // 老人姓名
    private String idCardNo; // 身份证号
    private String prepaidRechargeNo; // 编号
}
