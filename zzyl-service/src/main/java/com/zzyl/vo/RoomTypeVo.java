package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class RoomTypeVo extends BaseVo {

    private Long id; // 主键ID
    private String name; // 房型名称
    private Integer bedCount = 0; // 床位数量
    private Integer roomCount = 0; // 房间数量
    private BigDecimal price; // 床位费用
    private String creator; // 创建人
    private String introduction; // 介绍
    private String photo; // 照片
    private Integer status; // 状态，0：禁用，1：启用
}
