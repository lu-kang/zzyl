package com.zzyl.vo;

import lombok.Data;

import java.util.List;

@Data
public class DevicePropertyStatusVo {

    public PropertyStatusInfo list;

    @Data
    public static class PropertyStatusInfo {
        public List<PropertyStatus> propertyStatusInfo;
    }

    @Data
    public static class PropertyStatus {
        private String dataType;
        private String identifier;
        private String name;
        private String value;
        private String unit;
        private String time;
    }
}