package com.zzyl.vo;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(description = "合同信息")
public class CheckInContractVo extends BaseDto {

    private String name; // 合同名称
    private LocalDateTime signDate; // 签约时间
    private String memberName; // 丙方名称
    private String memberPhone; // 丙方手机号
    private String pdfUrl; // 合同pdf文件地址
}


