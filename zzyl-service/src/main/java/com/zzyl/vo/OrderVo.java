package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "订单信息")
public class OrderVo extends BaseVo {

    private Long tradingOrderNo; // 交易号
    private Integer paymentStatus; // 支付状态 1未付 2已付 3已关闭
    private BigDecimal amount; // 订单金额
    private BigDecimal refund; // 退款金额
    private String isRefund; // 是否退款
    private Long memberId; // 下单会员ID
    private Long projectId; // 项目ID
    private Long elderId; // 老人ID
    private LocalDateTime estimatedArrivalTime; // 预计到达时间
    private String orderNo; // 订单编码
    private String reason; // 原因
    private Integer status; // 订单状态 0待支付 1待执行 2已执行 3已完成 4已关闭 5已退款
    private ElderVo elderVo; // 老人信息
    private MemberVo memberVo; // 下单人信息
    private NursingProjectVo nursingProjectVo; // 护理项目
    private BedVo bedVo; // 床位
    private TradingVo tradingVo; // 支付信息
    private RefundRecordVo refundRecordVo; // 退款记录
    private NursingTaskVo nursingTaskVo; // 执行记录
}
