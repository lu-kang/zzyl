package com.zzyl.vo.retreat;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 欠款 月度欠费
 */
@Data
@ApiModel(description = "欠款(月度欠费)")
public class Arrearage {

    private String code; // 账单编码
    private int type; // 账单类型 (0:月度账单, 1:订单)
    private String billMonth; // 账单月份
    private BigDecimal amount; // 可退金额
}
