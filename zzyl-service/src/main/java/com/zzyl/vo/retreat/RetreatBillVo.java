package com.zzyl.vo.retreat;

import com.zzyl.vo.BalanceVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "退住账单")
public class RetreatBillVo {

    private List<DueBack> dueBackList; // 应退账单
    private List<Arrearage> arrearageList; // 月度欠费
    private BalanceVo balanceVo; // 余额（押金+预缴款）
    private List<Unpaid> unpaidList; // 订单未缴
}
