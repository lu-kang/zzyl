package com.zzyl.vo.retreat;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(description = "费用应退(月度账单+微服务的订单)")
public class DueBack {

    private String code; // 账单编码
    private int type; // 账单类型 (0:月度账单, 1:订单)
    private String billMonth; // 账单月份
    private BigDecimal amount; // 可退金额
    private String nursingName; // 护理项目
    private BigDecimal realAmount; // 实退金额
    private String remark; // 调整备注
    private Long tradingOrderNo; // 订单号
    private Integer surplusDay; // 退住天数
    private Integer realDay; // 实际天数
    private Integer dueBackDay; // 退住天数
}
