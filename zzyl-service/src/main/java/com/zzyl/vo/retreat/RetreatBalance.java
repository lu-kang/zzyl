package com.zzyl.vo.retreat;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(description = "余额(押金+预缴款)")
public class RetreatBalance {

    private BigDecimal depositAmount; // 押金
    private BigDecimal prepaidAmount; // 预缴款
}
