package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "入住配置信息")
public class CheckInConfigVo extends BaseVo {

    private Long elderId; // 老人ID
    private LocalDateTime checkInStartTime; // 入住开始时间
    private LocalDateTime checkInEndTime; // 入住结束时间
    private Long nursingLevelId; // 护理等级ID
    private String nursingLevelName; // 护理等级名称
    private String bedNumber; // 床位号
    private LocalDateTime costStartTime; // 费用开始时间
    private LocalDateTime costEndTime; // 费用结束时间
    private String checkInStartTimeStr; // 入住开始时间字符串格式
    private String checkInEndTimeStr; // 入住结束时间字符串格式
    private String costStartTimeStr; // 费用开始时间字符串格式
    private String costEndTimeStr; // 费用结束时间字符串格式
    private BigDecimal depositAmount; // 押金金额
    private BigDecimal nursingCost; // 护理费用
    private BigDecimal bedCost; // 床位费用
    private BigDecimal otherCost; // 其他费用
    private BigDecimal medicalInsurancePayment; // 医保支付
    private BigDecimal governmentSubsidy; // 政府补贴
    private BigDecimal monthCost; // 每月应付
    private BigDecimal add1; // 小计1
    private BigDecimal add2; // 小计2
    private Long roomId; // 房间ID
    private Long floorId; // 楼层ID
    private Long bedId; // 床位ID
    private String floorName; // 楼层名称
    private String code; // 房间编号
}
