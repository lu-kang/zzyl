package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DeviceDataVo extends BaseVo {

    private String iotId; // 物联网设备ID
    private String deviceName; // 设备名称
    private String nickname; // 备注名称
    private String productKey; // 产品ID
    private String productName; // 产品名称
    private String functionId; // 功能标识符
    private String accessLocation; // 访问位置
    private Integer locationType; // 位置类型 0：随身设备 1：固定设备
    private Integer physicalLocationType; // 物理位置类型 0楼层 1房间 2床位 -1老人
    private String deviceDescription; // 位置备注
    private String dataValue; // 数据值
    private LocalDateTime alarmTime; // 报警时间
}