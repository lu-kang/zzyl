package com.zzyl.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 退住详情响应模型
 */
@Data
public class RetreatDetailVo {

    private RetreatElderVo retreatElderVo; // 退住老人信息响应模型
    private LocalDateTime releaseDate; // 解除合同时间
    private String releasePdfUrl; // 解除合同url
    private String billJson; // 账单信息json
    private RefundVoucherVo refundVoucherVo; // 退款凭证响应模型
}
