package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(description = "老人入住信息响应模型")
public class ElderCheckInInfoVo {

    private Long id; // 老人id
    private String name; // 老人姓名
    private String idCardNo; // 身份证号
    private LocalDateTime checkInStartTime; // 入住开始时间
    private LocalDateTime checkInEndTime; // 入住结束时间
    private LocalDateTime costStartTime; // 费用开始时间
    private LocalDateTime costEndTime; // 费用结束时间
    private Long nursingLevelId; // 护理等级ID
    private String nursingLevelName; // 护理等级名称
    private String bedNumber; // 床位号
    private String nursingName; // 护理员姓名
    private String phone; // 手机号
    private String address; // 家庭住址
}
