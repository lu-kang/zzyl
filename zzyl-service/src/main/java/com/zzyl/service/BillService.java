package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.BillPageQueryDto;
import com.zzyl.vo.BillVo;
import com.zzyl.vo.PrepaidRechargeRecordVo;

public interface BillService {

    /**
     * 根据主键选择账单
     *
     * @param id 主键
     * @return 账单实体
     */
    BillVo selectByPrimaryKey(Long id);


    /**
     * 分页查询欠费账单
     *
     * @param bedNo     床位号
     * @param elderName 老人姓名
     * @param pageNum   页码
     * @param pageSize  每页数量
     * @return 分页结果
     */
    PageBean<BillVo> arrears(String bedNo, String elderName, Integer pageNum, Integer pageSize);

    /**
     * 分页查询预充值记录
     *
     * @param bedNo     床位号
     * @param elderName 老人姓名
     * @param pageNum   页码
     * @param pageSize  每页数量
     * @return 分页结果
     */
    PageBean<PrepaidRechargeRecordVo> prepaidRechargeRecordPage(String bedNo, String elderName, Integer pageNum, Integer pageSize);

    /**
     * 小程序账单分页查询
     *
     * @param dto
     * @return
     */
    PageBean<BillVo> pageQuery4Customer(BillPageQueryDto dto);

    /**
     * 管理端账单分页查询
     *
     * @param dto
     * @return
     */
    PageBean<BillVo> pageQuery4Admin(BillPageQueryDto dto);
}