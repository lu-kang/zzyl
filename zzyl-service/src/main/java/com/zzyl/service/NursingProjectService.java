package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.NursingProjectDto;
import com.zzyl.dto.NursingProjectPageQueryDto;
import com.zzyl.vo.NursingProjectVo;

import java.util.List;

public interface NursingProjectService {

    /**
     * 分页查询护理项目
     *
     * @param dto
     * @return
     */
    PageBean<NursingProjectVo> pageQuery(NursingProjectPageQueryDto dto);

    /**
     * 添加
     *
     * @param dto
     */
    void addNursingProject(NursingProjectDto dto);

    /**
     * 根据id查询护理项目
     *
     * @param id
     * @return
     */
    NursingProjectVo getById(Long id);

    /**
     * 修改护理项目
     *
     * @param dto
     */
    void updateNursingProject(NursingProjectDto dto);

    /**
     * 启用禁用
     *
     * @param id
     * @param status
     */
    void enableOrDisable(Long id, Integer status);

    /**
     * 根据id删除护理项目
     *
     * @param id
     */
    void deleteById(Long id);

    /**
     * 查询所有护理项目
     *
     * @return
     */
    List<NursingProjectVo> getAllNursingProject();
}
