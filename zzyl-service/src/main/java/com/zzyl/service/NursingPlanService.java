package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.NursingPlanDto;
import com.zzyl.dto.NursingPlanPageQueryDto;
import com.zzyl.vo.NursingPlanVo;

import java.util.List;

public interface NursingPlanService {

    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    PageBean<NursingPlanVo> pageQuery(NursingPlanPageQueryDto dto);

    /**
     * 添加护理计划
     *
     * @param dto
     */
    void addNursingPlan(NursingPlanDto dto);

    /**
     * 根据id查询护理计划详情信息
     *
     * @param id
     * @return
     */
    NursingPlanVo getDetailById(Long id);


    /**
     * 更新护理计划
     *
     * @param dto
     */
    void updateNursingPlan(NursingPlanDto dto);

    /**
     * 启用、禁用
     *
     * @param id
     * @param status
     */
    void enableOrDisable(Long id, Integer status);

    /**
     * 根据id删除
     *
     * @param id
     */
    void deleteById(Long id);

    /**
     * 查询所有护理计划
     *
     * @return
     */
    List<NursingPlanVo> getAll();

}

