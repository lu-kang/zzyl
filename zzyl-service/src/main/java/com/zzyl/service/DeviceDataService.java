package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.DeviceDataPageReqDto;
import com.zzyl.dto.QueryDeviceDataDto;
import com.zzyl.vo.DeviceDataGraphVo;
import com.zzyl.vo.DeviceDataVo;

import java.util.List;

public interface DeviceDataService {

    /**
     * 分页查询设备数据
     *
     * @param dto
     * @return
     */
    PageBean<DeviceDataVo> pageQueryDeviceData(DeviceDataPageReqDto dto);

    /**
     * 按日查询设备数据
     *
     * @param dto
     * @return
     */
    List<DeviceDataGraphVo> queryDeviceDataListByDay(QueryDeviceDataDto dto);
}
