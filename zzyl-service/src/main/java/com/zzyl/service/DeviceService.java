package com.zzyl.service;

import com.aliyun.iot20180120.models.DeleteDeviceRequest;
import com.aliyun.iot20180120.models.QueryDevicePropertyStatusRequest;
import com.aliyun.iot20180120.models.QueryThingModelPublishedRequest;
import com.zzyl.base.PageBean;
import com.zzyl.dto.DeviceDto;
import com.zzyl.dto.DevicePageQueryDto;
import com.zzyl.dto.QueryDeviceDataDto;
import com.zzyl.vo.*;

import java.util.List;

public interface DeviceService {

    /**
     * 同步物联网平台产品列表到Redis
     */
    void syncProductList2Redis();

    /**
     * 查询所有产品列表
     *
     * @return
     */
    List<ProductVo> allProduct();

    /**
     * 注册设备
     *
     * @param dto
     */
    void registerDevice(DeviceDto dto);

    /**
     * 分页查询设备
     *
     * @param dto
     * @return
     */
    PageBean<DeviceVo> pageQuery(DevicePageQueryDto dto);

    /**
     * 查询设备详细数据
     *
     * @param dto
     * @return
     */
    DeviceVo queryDeviceDetail(DeviceDto dto);


    /**
     * 查询指定设备的物模型运行状态
     *
     * @param request 查询设备的物模型运行状态请求模型
     * @return 物模型运行状态
     */
    DevicePropertyStatusVo queryDevicePropertyStatus(QueryDevicePropertyStatusRequest request);

    /**
     * 更新设备
     *
     * @param deviceDto 更新设备请求模型
     */
    void updateDevice(DeviceDto deviceDto);

    /**
     * 删除设备
     *
     * @param request 删除设备请求模型
     */
    void deleteDevice(DeleteDeviceRequest request);

    /**
     * 查看指定产品的已发布物模型中的功能定义详情
     *
     * @param request 查询已发布物模型请求模型
     * @return 已发布物模型中的功能定义详情
     */
    QueryThingModelVo queryThingModelPublished(QueryThingModelPublishedRequest request);

}
