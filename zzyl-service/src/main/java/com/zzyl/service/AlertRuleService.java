package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.AlertRuleDto;
import com.zzyl.dto.AlertRuleQueryDto;
import com.zzyl.vo.AlertRuleVo;

public interface AlertRuleService {

    /**
     * 新增报警规则
     *
     * @param alertRuleDto 报警规则请求模型
     */
    void createRule(AlertRuleDto alertRuleDto);

    /**
     * 分页条件查询
     */
    PageBean<AlertRuleVo> getAlertRulePage(AlertRuleQueryDto dto);

    /**
     * 删除报警
     *
     * @param id 报警规则id
     */
    void deleteAlertRule(Long id);

    /**
     * 修改报警
     *
     * @param id           报警规则id
     * @param alertRuleDto 报警规则更新请求模型
     */
    void updateAlertRule(Long id, AlertRuleDto alertRuleDto);

    /**
     * 查询报警信息
     *
     * @param id 报警规则id
     * @return 报警信息
     */
    AlertRuleVo readAlertRule(Long id);

    /**
     * 启用或禁用
     *
     * @param id     报警id
     * @param status 报警规则状态
     */
    void enableOrDisable(Long id, Integer status);
}
