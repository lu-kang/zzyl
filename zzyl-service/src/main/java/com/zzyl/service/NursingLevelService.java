package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.NursingLevelDto;
import com.zzyl.dto.NursingLevelPageQueryDto;
import com.zzyl.vo.NursingLevelVo;

import java.util.List;

public interface NursingLevelService {

    /**
     * 分页查询护理级别列表
     */
    PageBean<NursingLevelVo> pageQuery(NursingLevelPageQueryDto dto);

    /**
     * 新增护理级别
     *
     * @param dto 护理级别
     */
    void addNursingLevel(NursingLevelDto dto);

    /**
     * 根据ID查询护理级别
     *
     * @param id 护理级别ID
     * @return 护理级别
     */
    NursingLevelVo getDetailById(Long id);

    /**
     * 更新护理级别
     *
     * @param dto 护理级别
     */
    void updateNursingLevel(NursingLevelDto dto);

    /**
     * 删除护理级别
     *
     * @param id 护理级别ID
     */
    void deleteById(Long id);


    /**
     * 启用或禁用
     *
     * @param id     ID
     * @param status 状态
     */
    void enableOrDisable(Long id, Integer status);

    /**
     * 查询全部护理级别
     *
     * @return 全部护理级别
     */
    List<NursingLevelVo> getAll();
}
