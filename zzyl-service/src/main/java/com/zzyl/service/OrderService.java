package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.OrderPageQueryDto;
import com.zzyl.entity.Order;
import com.zzyl.vo.OrderVo;

import java.util.List;

public interface OrderService {

    /**
     * 根据订单ID获取订单信息
     *
     * @param orderId 订单ID
     * @return 订单信息
     */
    OrderVo getOrderById(Long orderId);


    /**
     * 根据客户id查询订单
     *
     * @param id
     * @return
     */
    List<OrderVo> listByMemberId(Long id);

    /**
     * 根据状态查询订单
     *
     * @param status
     * @return
     */
    List<Order> selectByStatus(Integer status);

    /**
     * 小程序端订单分页查询
     *
     * @param dto
     * @return
     */
    PageBean<OrderVo> pageQuery4Customer(OrderPageQueryDto dto);

    /**
     * 管理端订单分页查询
     *
     * @param dto
     * @return
     */
    PageBean<OrderVo> pageQuery4Admin(OrderPageQueryDto dto);
}

