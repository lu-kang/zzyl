package com.zzyl.service;


import com.zzyl.dto.FloorDto;
import com.zzyl.vo.FloorVo;

import java.util.List;

public interface FloorService {
    /**
     * 添加楼层
     *
     * @param floorDto 楼层信息
     */
    void addFloor(FloorDto floorDto);

    /**
     * 删除楼层
     *
     * @param id 楼层id
     */
    void deleteFloor(Long id);

    /**
     * 更新楼层
     *
     * @param floorDto 楼层信息
     */
    void updateFloor(FloorDto floorDto);

    /**
     * 获取楼层信息
     *
     * @param id 楼层id
     * @return 楼层信息
     */
    FloorVo getFloor(Long id);

    /**
     * 获取所有楼层信息
     *
     * @return 所有楼层信息
     */
    List<FloorVo> getAllFloors();

    /**
     * 查询所有房间和床位
     *
     * @return
     */
    List<FloorVo> getAllWithRoomAndBed();

    /**
     * 查询所有楼层
     *
     * @return
     */
    List<FloorVo> selectAllByNur();

    /**
     * 根据床位状态查询获取所有楼层数据
     *
     * @param status 床位状态{@link com.zzyl.enums.BedStatusEnum}
     * @return 所有楼层数据
     */
    List<FloorVo> getRoomAndBedByBedStatus(Integer status);

    /**
     * 获取所有带智能设备的楼层
     *
     * @return
     */
    List<FloorVo> getAllFloorsWithDevice();

}
