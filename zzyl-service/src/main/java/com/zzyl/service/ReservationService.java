package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.ReservationDto;
import com.zzyl.dto.ReservationQueryDto;
import com.zzyl.vo.ReservationVo;
import com.zzyl.vo.TimeCountVo;

import java.util.List;

public interface ReservationService {

    /**
     * 分页查找预约信息（小程序端）
     */
    PageBean<ReservationVo> findByPage4Customer(ReservationQueryDto dto);

    /**
     * 分页查找预约（管理端）
     */
    PageBean<ReservationVo> findByPage4Admin(ReservationQueryDto dto);

    /**
     * 查询用户当天取消的预约次数
     *
     * @return
     */
    Integer cancelledCount();

    /**
     * 查询时间段内的预约次数
     *
     * @param time
     * @return
     */
    List<TimeCountVo> countByTime(Long time);

    /**
     * 添加预约
     *
     * @param dto
     */
    void addReservation(ReservationDto dto);


    /**
     * 取消预约
     *
     * @param id
     */
    void cancelReservation(Long id);

}
