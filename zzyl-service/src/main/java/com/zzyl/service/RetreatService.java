package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.vo.RetreatDetailVo;
import com.zzyl.vo.RetreatPageQueryVo;

import java.time.LocalDateTime;

public interface RetreatService {

    /**
     * 分页查询退住信息
     *
     * @param elderName        老人姓名，模糊查询
     * @param elderIdCardNo    身份证号，精确查询
     * @param retreatStartTime 退住开始时间，格式：yyyy-MM-dd HH:mm:ss
     * @param retreatEndTime   退住结束时间，格式：yyyy-MM-dd HH:mm:ss
     * @param pageNum          页码
     * @param pageSize         页面大小
     * @return 分页结果
     */
    PageBean<RetreatPageQueryVo> pageQuery(String elderName, String elderIdCardNo, LocalDateTime retreatStartTime, LocalDateTime retreatEndTime, Integer pageNum, Integer pageSize);

    /**
     * 根据退住id查询详情
     *
     * @param id 退住id
     * @return 退住详情
     */
    RetreatDetailVo detail(Long id);
}
