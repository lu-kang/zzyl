package com.zzyl.service;

import com.zzyl.dto.BedDto;
import com.zzyl.vo.BedVo;

import java.util.List;

public interface BedService {

    /**
     * 通过房间ID检索床位
     *
     * @param roomId 房间ID
     * @return 床位视图对象列表
     */
    List<BedVo> getBedsByRoomId(Long roomId);

    /**
     * 创建床位
     *
     * @param dto
     */
    void createBed(BedDto dto);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    BedVo getById(Long id);

    /**
     * 根据id更新床位
     *
     * @param dto
     */
    void updateBed(BedDto dto);

    /**
     * 根据id删除
     *
     * @param id
     */
    void deleteById(Long id);
}
