package com.zzyl.service;

import com.zzyl.base.PageBean;
import com.zzyl.dto.AlertDataHandleDto;
import com.zzyl.dto.AlertDataPageQueryDto;
import com.zzyl.vo.AlertDataVo;

public interface AlertDataService {

    /**
     * 分页条件查询报警数据
     *
     * @param dto
     * @return
     */
    PageBean<AlertDataVo> pageQuery(AlertDataPageQueryDto dto);

    /**
     * 处理报警数据
     *
     * @param id
     * @param dto
     */
    void handleAlertData(Long id, AlertDataHandleDto dto);
}
