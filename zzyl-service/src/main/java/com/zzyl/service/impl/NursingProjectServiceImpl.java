package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.NursingProjectDto;
import com.zzyl.dto.NursingProjectPageQueryDto;
import com.zzyl.entity.NursingProject;
import com.zzyl.enums.BasicEnum;
import com.zzyl.exception.BaseException;
import com.zzyl.mapper.NursingProjectMapper;
import com.zzyl.service.NursingProjectService;
import com.zzyl.vo.NursingProjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class NursingProjectServiceImpl implements NursingProjectService {

    @Autowired
    NursingProjectMapper nursingProjectMapper;

    /**
     * 分页查询护理项目
     *
     * @param dto
     * @return
     */
    @Override
    public PageBean<NursingProjectVo> pageQuery(NursingProjectPageQueryDto dto) {
        //开启分页
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        //调用mapper查询数据
        Page<NursingProjectVo> page = nursingProjectMapper.pageQuery(dto);

        //创建PageBean对象，将List集合，总条数封装进去
        /*PageBean<NursingProjectVo> pageBean = new PageBean<>();

        pageBean.setRecords(page.getResult());
        pageBean.setTotal(page.getTotal());*/

        return PageBean.of(page, NursingProjectVo.class);
    }

    /**
     * 添加
     *
     * @param dto
     */
    @Override
    public void addNursingProject(NursingProjectDto dto) {
        //将dto转换成pojo
        NursingProject nursingProject = BeanUtil.toBean(dto, NursingProject.class);

        //补全属性（时间、创建、状态...）
        nursingProject.setCreateTime(LocalDateTime.now());
        nursingProject.setUpdateTime(LocalDateTime.now());

        nursingProject.setCreateBy(1671403256519078138L);
        nursingProject.setUpdateBy(1671403256519078138L);

        //添加数据
        nursingProjectMapper.insert(nursingProject);
    }

    /**
     * 根据id查询护理项目
     *
     * @param id
     * @return
     */
    @Override
    public NursingProjectVo getById(Long id) {
        NursingProject nursingProject = nursingProjectMapper.getById(id);
        return BeanUtil.toBean(nursingProject, NursingProjectVo.class);
    }

    /**
     * 修改护理项目
     *
     * @param dto
     */
    @Override
    public void updateNursingProject(NursingProjectDto dto) {
        //1. 将dto转换成pojo
        NursingProject nursingProject = BeanUtil.toBean(dto, NursingProject.class);

        //2. 补全属性信息
        nursingProject.setUpdateTime(LocalDateTime.now());
        nursingProject.setUpdateBy(1671403256519078138L);

        //3. 调用mapper操作数据库
        nursingProjectMapper.update(nursingProject);
    }

    /**
     * 启用禁用
     *
     * @param id
     * @param status
     */
    @Override
    public void enableOrDisable(Long id, Integer status) {
        //mapper方法：主要是CRUD，不涉及任何业务逻辑，select get list query add save insert update delete remove page
        NursingProject nursingProject = NursingProject.builder()
                .id(id)
                .status(status)
                .updateTime(LocalDateTime.now())
                .updateBy(1671403256519078138L)
                .build();
        nursingProjectMapper.update(nursingProject);
    }

    /**
     * 根据id删除护理项目
     *
     * @param id
     */
    @Override
    public void deleteById(Long id) {
        //判断护理项目的状态
        NursingProject nursingProject = nursingProjectMapper.getById(id);

        if (nursingProject == null) {
            //抛异常
            throw new RuntimeException("查询不到对应的护理项目，请求参数非法！");
        }

        if (nursingProject.getStatus() == 1) {
            //抛异常
            throw new BaseException(BasicEnum.ENABLED_CANNOT_DELETED);
        }

        nursingProjectMapper.deleteById(id);
    }

    /**
     * 查询所有护理项目
     *
     * @return
     */
    @Override
    public List<NursingProjectVo> getAllNursingProject() {
        List<NursingProject> nursingProjects = nursingProjectMapper.selectAll();
        return BeanUtil.copyToList(nursingProjects, NursingProjectVo.class);
    }
}
