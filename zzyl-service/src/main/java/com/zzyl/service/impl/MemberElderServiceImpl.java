package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.MemberElderDto;
import com.zzyl.entity.Elder;
import com.zzyl.entity.MemberElder;
import com.zzyl.enums.BasicEnum;
import com.zzyl.exception.BaseException;
import com.zzyl.mapper.ElderMapper;
import com.zzyl.mapper.MemberElderMapper;
import com.zzyl.properties.JwtTokenProperties;
import com.zzyl.service.MemberElderService;
import com.zzyl.utils.ThreadLocalUtil;
import com.zzyl.vo.MemberElderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MemberElderServiceImpl implements MemberElderService {

    @Autowired
    private MemberElderMapper memberElderMapper;

    /**
     * 更新客户老人关联
     *
     * @param memberElder 客户老人关联实体
     */
    @Override
    public void update(MemberElderDto memberElder) {
        MemberElder memberElder1 = BeanUtil.toBean(memberElder, MemberElder.class);
        memberElderMapper.update(memberElder1);
    }

    /**
     * 根据ID删除客户老人关联
     *
     * @param id 客户老人关联ID
     */
    @Override
    public void deleteById(Long id) {
        memberElderMapper.deleteById(id);
    }

    /**
     * 根据ID获取客户老人关联
     *
     * @param id 客户老人关联ID
     * @return 客户老人关联实体
     */
    @Override
    public MemberElderVo getById(Long id) {
        MemberElder byId = memberElderMapper.getById(id);
        return BeanUtil.toBean(byId, MemberElderVo.class);
    }

    /**
     * 根据客户ID获取客户老人关联列表
     *
     * @param memberId 客户ID
     * @return 客户老人关联列表
     */
    @Override
    public List<MemberElderVo> listByMemberId(Long memberId) {
        List<MemberElder> memberElders = memberElderMapper.listByMemberId(memberId);
        return BeanUtil.copyToList(memberElders, MemberElderVo.class);
    }

    /**
     * 根据老人ID获取客户老人关联列表
     *
     * @param elderId 老人ID
     * @return 客户老人关联列表
     */
    @Override
    public List<MemberElderVo> listByElderId(Long elderId) {
        List<MemberElder> memberElders = memberElderMapper.listByElderId(elderId);
        return BeanUtil.copyToList(memberElders, MemberElderVo.class);
    }

    /**
     * 分页查询客户老人关联列表
     *
     * @param memberId 客户ID
     * @param elderId  老人ID
     * @param pageNum  当前页码
     * @param pageSize 每页显示数量
     * @return 分页结果
     */
    @Override
    public PageBean<MemberElderVo> listByPage(Long memberId, Long elderId, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        if (ObjectUtil.isEmpty(memberId)) {
            memberId = ThreadLocalUtil.get();
        }
        Page<MemberElder> page = memberElderMapper.listByCondition(memberId, elderId);
        return PageBean.of(page, MemberElderVo.class);
    }

    @Override
    public List<MemberElderVo> myElderList() {
        Long userId = ThreadLocalUtil.get();
        List<MemberElder> memberElders = memberElderMapper.listByMemberId(userId);
        return BeanUtil.copyToList(memberElders, MemberElderVo.class);
    }

    @Resource
    private ElderMapper elderMapper;

    /**
     * 添加客户老人关联
     *
     * @param dto 客户老人关联实体
     */
    @Override
    public void add(MemberElderDto dto) {
        //根据前端携带的身份证和姓名查询老人是否存在
        Elder elder = elderMapper.selectByIdCardAndName(dto.getIdCard(), dto.getName());
        if (ObjectUtil.isEmpty(elder) || ObjectUtil.notEqual(elder.getStatus(), 0)) { //0入住中
            //抛异常：老人不存在
            throw new BaseException(BasicEnum.ELDER_NOT_EXIST);
        }

        MemberElder memberElder = BeanUtil.toBean(dto, MemberElder.class);
        memberElder.setMemberId(ThreadLocalUtil.get());
        memberElder.setElderId(elder.getId());
        memberElderMapper.add(memberElder);
    }
}

