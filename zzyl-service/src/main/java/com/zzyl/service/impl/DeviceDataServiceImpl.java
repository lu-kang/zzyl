package com.zzyl.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.DeviceDataPageReqDto;
import com.zzyl.dto.QueryDeviceDataDto;
import com.zzyl.mapper.DeviceDataMapper;
import com.zzyl.service.DeviceDataService;
import com.zzyl.vo.DeviceDataGraphVo;
import com.zzyl.vo.DeviceDataVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeviceDataServiceImpl implements DeviceDataService {

    final DeviceDataMapper deviceDataMapper;

    /**
     * 分页查询设备数据
     *
     * @param dto
     * @return
     */
    @Override
    public PageBean<DeviceDataVo> pageQueryDeviceData(DeviceDataPageReqDto dto) {
        //开启分页
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());

        //将startTime的时间赋值给startDate
        dto.setStartDate(LocalDateTimeUtil.of(dto.getStartTime()));
        dto.setEndDate(LocalDateTimeUtil.of(dto.getEndTime()));

        Page<DeviceDataVo> deviceDataVos = deviceDataMapper.selectByPage(dto);
        return PageBean.of(deviceDataVos, DeviceDataVo.class);
    }


    /**
     * 按日查询设备数据
     *
     * @param dto
     * @return
     */
    @Override
    public List<DeviceDataGraphVo> queryDeviceDataListByDay(QueryDeviceDataDto dto) {
        //处理日期
        dto.setStartDate(LocalDateTimeUtil.of(dto.getStartTime()));
        dto.setEndDate(LocalDateTimeUtil.of(dto.getEndTime()));
        return deviceDataMapper.queryDeviceDataListByDay(dto);
    }

}
