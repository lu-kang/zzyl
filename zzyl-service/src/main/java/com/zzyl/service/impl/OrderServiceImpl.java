package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.OrderPageQueryDto;
import com.zzyl.entity.Order;
import com.zzyl.mapper.OrderMapper;
import com.zzyl.service.OrderService;
import com.zzyl.utils.ThreadLocalUtil;
import com.zzyl.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 根据订单id获取订单详情
     *
     * @param orderId 订单id
     * @return 订单vo
     */
    @Override
    public OrderVo getOrderById(Long orderId) {
        Order order = orderMapper.selectByPrimaryKey(orderId);
        return BeanUtil.toBean(order, OrderVo.class);
    }

    @Override
    public List<OrderVo> listByMemberId(Long id) {
        return orderMapper.listByMemberId(id);
    }

    @Override
    public List<Order> selectByStatus(Integer status) {
        return orderMapper.selectByStatus(status);
    }

    /**
     * 小程序端订单分页查询
     *
     * @param dto
     * @return
     */
    @Override
    public PageBean<OrderVo> pageQuery4Customer(OrderPageQueryDto dto) {
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        //设置用户id
        dto.setUserId(ThreadLocalUtil.get());
        Page<Order> orders = orderMapper.searchOrders(dto);
        return PageBean.of(orders, OrderVo.class);
    }

    /**
     * 管理端订单分页查询
     *
     * @param dto
     * @return
     */
    @Override
    public PageBean<OrderVo> pageQuery4Admin(OrderPageQueryDto dto) {
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        Page<Order> orders = orderMapper.searchOrders(dto);
        return PageBean.of(orders, OrderVo.class);
    }
}

