package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.zzyl.dto.BedDto;
import com.zzyl.entity.Bed;
import com.zzyl.mapper.BedMapper;
import com.zzyl.service.BedService;
import com.zzyl.vo.BedVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BedServiceImpl implements BedService {

    @Autowired
    private BedMapper bedMapper;

    @Override
    public List<BedVo> getBedsByRoomId(Long roomId) {
        return bedMapper.getBedsByRoomId(roomId);
    }

    /**
     * 创建床位
     *
     * @param dto
     */
    @Override
    public void createBed(BedDto dto) {
        //1. 将BedDto转换成Bed实体对象   (拷贝原则：属性对应，类型相同)
        Bed bed = BeanUtil.toBean(dto, Bed.class);

        //补全Bed数据（状态）
        bed.setBedStatus(0);//0未入住 1已入住

        bed.setCreateTime(LocalDateTime.now());
        bed.setUpdateTime(LocalDateTime.now());

        bed.setCreateBy(1671403256519078138L); //应该解析jwt，由于没有登录功能，暂时默认admin
        bed.setUpdateBy(1671403256519078138L);

        //2. 调用BedMapper添加数据   insert save add    update  delete get find   getByusername
        bedMapper.insert(bed);
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Override
    public BedVo getById(Long id) {
        //根据id查询
        Bed bed = bedMapper.getById(id);
        //将bed对象转换成bedVo对象
        return BeanUtil.toBean(bed, BedVo.class);
    }

    /**
     * 根据id更新床位
     *
     * @param dto
     */
    @Override
    public void updateBed(BedDto dto) {
        //1. 将dto转化成pojo
        Bed bed = BeanUtil.toBean(dto, Bed.class);
        //2. 补全字段信息
        bed.setUpdateTime(LocalDateTime.now());
        bed.setUpdateBy(1671403256519078138L);

        bed.setBedStatus(null);
        //3. 更新操作
        bedMapper.updateById(bed);
    }

    /**
     * 根据id删除
     *
     * @param id
     */
    @Override
    public void deleteById(Long id) {
        bedMapper.deleteById(id);
    }
}

