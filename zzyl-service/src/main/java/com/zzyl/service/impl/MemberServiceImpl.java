package com.zzyl.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.UserLoginRequestDto;
import com.zzyl.entity.Member;
import com.zzyl.mapper.MemberMapper;
import com.zzyl.properties.JwtTokenProperties;
import com.zzyl.properties.WeChatProperties;
import com.zzyl.service.*;
import com.zzyl.utils.JwtUtil;
import com.zzyl.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MemberServiceImpl implements MemberService {

    private static final List<String> DEFAULT_NICKNAME_PREFIX =
            List.of("生活更美好", "大桔大利", "日富一日", "好柿开花", "柿柿如意", "一椰暴富", "大柚所为", "杨梅吐气", "天生荔枝");

    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private ContractService contractService;
    @Autowired
    private MemberElderService memberElderService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private JwtTokenProperties jwtTokenProperties;
    @Autowired
    private WeChatProperties weChatProperties;
    @Autowired
    private WechatService wechatService;

    /**
     * 根据id查询用户
     *
     * @param id 用户id
     * @return 用户信息
     */
    @Override
    public Member getById(Long id) {
        return memberMapper.selectById(id);
    }

    /**
     * 分页查询用户列表
     *
     * @param page     当前页码
     * @param pageSize 每页数量
     * @param phone    手机号
     * @param nickname 昵称
     * @return 分页结果
     */
    @Override
    public PageBean<MemberVo> page(Integer page, Integer pageSize, String phone, String nickname) {
        PageHelper.startPage(page, pageSize);
        Page<Member> listPage = memberMapper.page(phone, nickname);

        PageBean<MemberVo> pageBean = PageBean.of(listPage, MemberVo.class);

        pageBean.getRecords().forEach(memberVo -> {
            List<ContractVo> contractVos = contractService.listByMemberPhone(memberVo.getPhone());
            memberVo.setIsSign(contractVos.isEmpty() ? "否" : "是");
            List<OrderVo> orderVos = orderService.listByMemberId(memberVo.getId());
            memberVo.setOrderCount(orderVos.size());
            List<MemberElderVo> memberElderVos = memberElderService.listByMemberId(memberVo.getId());
            List<String> collect = memberElderVos.stream().map(m -> m.getElderVo().getName()).collect(Collectors.toList());
            memberVo.setElderNames(String.join(",", collect));
        });
        return pageBean;
    }

    /**
     * 微信小程序登录
     *
     * @param dto
     * @return
     */
    @Override
    public WxLoginVo wxLogin(UserLoginRequestDto dto) {

        //1. 调用微信平台的登录接口，通过code换取openid
        String openid = wechatService.getOpenid(dto.getCode());

        //2. 调用微信平台的获取手机号接口，通过phoneCode换取phone
        String phone = wechatService.getPhone(dto.getPhoneCode());

        //3. 根据openid去查询member表，判断是否为新用户
        Member member = memberMapper.getByOpenId(openid);
        //如果对象为空，需要添加一条数据
        if (ObjectUtil.isEmpty(member)) {  // null
            //先创建对象
            member = new Member();

            member.setOpenId(openid);//openid
            member.setName(RandomUtil.randomEle(DEFAULT_NICKNAME_PREFIX) + RandomUtil.randomNumbers(4));//昵称
            member.setPhone(phone);
            //member.setCreateTime(LocalDateTime.now());
            //member.setUpdateTime(LocalDateTime.now());
            memberMapper.save(member);//注意：主键自增返回

        }

        //4. 给用户生成token令牌（jwt中只存放用户id）
        Map<String, Object> claims = new HashMap<String, Object>();
        claims.put("id", member.getId()); //存放用户id

        //生成jwt令牌
        String jwt = JwtUtil.createJWT(jwtTokenProperties.getSecretKey(),
                jwtTokenProperties.getTtl(),
                claims);

        //5. 返回VO数据
        return WxLoginVo.builder()
                .token(jwt)
                .nickName(member.getName())
                .build();
    }


}
