package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.NursingLevelDto;
import com.zzyl.dto.NursingLevelPageQueryDto;
import com.zzyl.entity.NursingLevel;
import com.zzyl.mapper.NursingLevelMapper;
import com.zzyl.service.NursingLevelService;
import com.zzyl.vo.NursingLevelVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class NursingLevelServiceImpl implements NursingLevelService {

    @Autowired
    NursingLevelMapper nursingLevelMapper;

    /**
     * 分页查询护理级别列表
     *
     * @param dto
     */
    @Override
    public PageBean<NursingLevelVo> pageQuery(NursingLevelPageQueryDto dto) {
        // 使用pagehelper进行分页查询护理等级数据
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        Page<NursingLevel> page = nursingLevelMapper.pageQuery(dto);
        // 封装分页信息
        return PageBean.of(page, NursingLevelVo.class);
    }

    /**
     * 新增护理级别
     *
     * @param dto 护理级别
     */
    @Override
    public void addNursingLevel(NursingLevelDto dto) {
        NursingLevel level = BeanUtil.toBean(dto, NursingLevel.class);
        nursingLevelMapper.insert(level);
    }

    /**
     * 根据ID查询护理级别
     *
     * @param id 护理级别ID
     * @return 护理级别
     */
    @Override
    public NursingLevelVo getDetailById(Long id) {
        NursingLevel level = nursingLevelMapper.getById(id);
        return BeanUtil.toBean(level, NursingLevelVo.class);
    }

    /**
     * 更新护理级别
     *
     * @param dto 护理级别
     */
    @Override
    public void updateNursingLevel(NursingLevelDto dto) {
        NursingLevel level = BeanUtil.toBean(dto, NursingLevel.class);
        nursingLevelMapper.update(level);
    }

    /**
     * 删除护理级别
     *
     * @param id 护理级别ID
     */
    @Override
    public void deleteById(Long id) {
        NursingLevel nursingLevel = nursingLevelMapper.getById(id);
        if (nursingLevel == null) {
            //说明id数据有问题
            throw new RuntimeException("请求参数有误，操作失败!");
        }

        if (ObjectUtil.equals(nursingLevel.getStatus(), 1)) {
            //说明状态是启用，禁止删除
            throw new RuntimeException("护理等级状态已启用，禁止删除!");
        }
        nursingLevelMapper.deleteById(id);
    }

    /**
     * 启用或禁用
     *
     * @param id     ID
     * @param status 状态
     */
    @Override
    public void enableOrDisable(Long id, Integer status) {
        NursingLevel level = NursingLevel.builder()
                .id(id)
                .status(status)
                .updateBy(1671403256519078138L)
                .updateTime(LocalDateTime.now())
                .build();
        nursingLevelMapper.update(level);
    }

    /**
     * 查询全部护理级别
     *
     * @return 全部护理级别
     */
    @Override
    public List<NursingLevelVo> getAll() {
        return nursingLevelMapper.getAll();
    }
}


