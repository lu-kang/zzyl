package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.CharSequenceUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.entity.Contract;
import com.zzyl.entity.Retreat;
import com.zzyl.entity.RetreatBill;
import com.zzyl.mapper.ContractMapper;
import com.zzyl.mapper.RetreatBillMapper;
import com.zzyl.mapper.RetreatMapper;
import com.zzyl.service.RetreatService;
import com.zzyl.vo.RefundVoucherVo;
import com.zzyl.vo.RetreatDetailVo;
import com.zzyl.vo.RetreatElderVo;
import com.zzyl.vo.RetreatPageQueryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class RetreatServiceImpl implements RetreatService {

    @Autowired
    private ContractMapper contractMapper;
    @Autowired
    private RetreatMapper retreatMapper;
    @Autowired
    private RetreatBillMapper retreatBillMapper;

    /**
     * 分页查询退住信息
     *
     * @param elderName        老人姓名，模糊查询
     * @param elderIdCardNo    身份证号，精确查询
     * @param retreatStartTime 退住开始时间，格式：yyyy-MM-dd HH:mm:ss
     * @param retreatEndTime   退住结束时间，格式：yyyy-MM-dd HH:mm:ss
     * @param pageNum          页码
     * @param pageSize         页面大小
     * @return 分页结果
     */
    @Override
    public PageBean<RetreatPageQueryVo> pageQuery(String elderName, String elderIdCardNo, LocalDateTime retreatStartTime, LocalDateTime retreatEndTime, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        //老人姓名，模糊查询，这里进行预处理
        elderName = CharSequenceUtil.isBlank(elderName) ? null : "%" + elderName + "%";
        Page<RetreatPageQueryVo> pageResult = retreatMapper.pageQuery(elderName, elderIdCardNo, retreatStartTime, retreatEndTime);
        return PageBean.of(pageResult, RetreatPageQueryVo.class);
    }

    /**
     * 根据退住id查询详情
     *
     * @param id 退住id
     * @return 退住详情
     */
    @Override
    public RetreatDetailVo detail(Long id) {
        //退住信息
        Retreat retreat = retreatMapper.selectById(id);

        //合同信息
        Contract contract = contractMapper.selectByContractNo(retreat.getContractNo());

        //退住账单
        RetreatBill retreatBill = retreatBillMapper.selectByRetreatId(id);

        //封装退住详情
        RetreatDetailVo retreatDetailVo = new RetreatDetailVo();
        retreatDetailVo.setRetreatElderVo(BeanUtil.toBean(retreat, RetreatElderVo.class));
        retreatDetailVo.setReleaseDate(contract.getReleaseDate());
        retreatDetailVo.setReleasePdfUrl(contract.getReleasePdfUrl());
        retreatDetailVo.setBillJson(retreatBill.getBillJson());
        retreatDetailVo.setRefundVoucherVo(BeanUtil.toBean(retreatBill, RefundVoucherVo.class));
        return retreatDetailVo;
    }
}
