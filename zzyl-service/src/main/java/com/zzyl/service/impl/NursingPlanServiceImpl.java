package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.NursingPlanDto;
import com.zzyl.dto.NursingPlanPageQueryDto;
import com.zzyl.dto.NursingProjectPlanDto;
import com.zzyl.entity.NursingPlan;
import com.zzyl.entity.NursingProjectPlan;
import com.zzyl.mapper.NursingPlanMapper;
import com.zzyl.mapper.NursingProjectPlanMapper;
import com.zzyl.service.NursingPlanService;
import com.zzyl.vo.NursingPlanVo;
import com.zzyl.vo.NursingProjectPlanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class NursingPlanServiceImpl implements NursingPlanService {

    @Autowired
    NursingPlanMapper nursingPlanMapper;
    @Autowired
    NursingProjectPlanMapper nursingProjectPlanMapper;


    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    @Override
    public PageBean<NursingPlanVo> pageQuery(NursingPlanPageQueryDto dto) {
        //开启分页查询
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());

        //调用mapper查询
        Page<NursingPlanVo> page = nursingPlanMapper.pageQuery(dto);

        //将page的信息封装到pageBena中
        return PageBean.of(page, NursingPlanVo.class);
    }

    /**
     * 添加护理计划
     *
     * @param dto
     */
    @Override
    @Transactional
    public void addNursingPlan(NursingPlanDto dto) {

        //1. 添加护理计划，操作 nursing_plan 表   NursingPlanMapper
        NursingPlan nursingPlan = BeanUtil.toBean(dto, NursingPlan.class);
        nursingPlan.setStatus(1);//1启用 0禁用
        nursingPlan.setCreateTime(LocalDateTime.now());
        nursingPlan.setUpdateTime(LocalDateTime.now());
        nursingPlan.setCreateBy(1671403256519078138L);
        nursingPlan.setUpdateBy(1671403256519078138L);
        nursingPlanMapper.insert(nursingPlan); //注意：添加时，主键自增返回


        //2. 添加护理计划护理项目的关系，操作 nursing_project_plan 表   NursingProjectPlanMapper
        List<NursingProjectPlanDto> projectPlans = dto.getProjectPlans();
        if (CollUtil.isEmpty(projectPlans)) {
            return;
        }

        //遍历集合，添加多条关系
        projectPlans.forEach(nursingProjectPlanDto -> {
            //将 nursingProjectPlanDto 转换成 nursingProjectPlan
            NursingProjectPlan projectPlan = BeanUtil.toBean(nursingProjectPlanDto, NursingProjectPlan.class);
            //关联护理计划的id
            projectPlan.setPlanId(nursingPlan.getId());
            projectPlan.setCreateTime(LocalDateTime.now());
            projectPlan.setUpdateTime(LocalDateTime.now());
            projectPlan.setCreateBy(1671403256519078138L);
            projectPlan.setUpdateBy(1671403256519078138L);
            nursingProjectPlanMapper.insert(projectPlan);
        });

    }

    /**
     * 根据id查询护理计划详情信息
     *
     * @param id
     * @return
     */
    @Override
    public NursingPlanVo getDetailById(Long id) {
        //NursingPlanVo = NursingPlanVO + List<中间表对象>

        //1. 先根据id查询护理计划
        NursingPlanVo nursingPlanVO = nursingPlanMapper.getById(id);

        //2. 根据护理计划id查询关系数据List
        List<NursingProjectPlanVo> nursingProjectPlanVoList = nursingProjectPlanMapper.getByPlanId(id);

        //3. 将两部分数据合并到Vo中
        nursingPlanVO.setProjectPlans(nursingProjectPlanVoList);

        return nursingPlanVO;
    }

    /**
     * 更新护理计划
     *
     * @param dto
     */
    @Override
    @Transactional
    public void updateNursingPlan(NursingPlanDto dto) {
        //思路：更新护理计划nursing_plan，更新护理计划项目关系nursing_project_plan

        NursingPlan nursingPlan = BeanUtil.toBean(dto, NursingPlan.class);
        nursingPlan.setUpdateTime(LocalDateTime.now());
        nursingPlan.setUpdateBy(1671403256519078138L);
        nursingPlanMapper.updateById(nursingPlan);

        //更新护理计划项目关系数据（先删除再添加）
        nursingProjectPlanMapper.deleteByPlanId(dto.getId());

        //2. 添加护理计划护理项目的关系，操作 nursing_project_plan 表   NursingProjectPlanMapper
        List<NursingProjectPlanDto> projectPlans = dto.getProjectPlans();
        if (CollUtil.isEmpty(projectPlans)) {
            return;
        }

        //遍历集合，添加多条关系
        projectPlans.forEach(nursingProjectPlanDto -> {
            //将 nursingProjectPlanDto 转换成 nursingProjectPlan
            NursingProjectPlan projectPlan = BeanUtil.toBean(nursingProjectPlanDto, NursingProjectPlan.class);
            //关联护理计划的id
            projectPlan.setPlanId(nursingPlan.getId());
            projectPlan.setCreateTime(LocalDateTime.now());
            projectPlan.setUpdateTime(LocalDateTime.now());
            projectPlan.setCreateBy(1671403256519078138L);
            projectPlan.setUpdateBy(1671403256519078138L);
            nursingProjectPlanMapper.insert(projectPlan);
        });
    }

    /**
     * 启用、禁用
     *
     * @param id
     * @param status
     */
    @Override
    public void enableOrDisable(Long id, Integer status) {
        NursingPlan nursingPlan = NursingPlan.builder()
                .id(id)
                .status(status)
                .updateTime(LocalDateTime.now())
                .updateBy(1671403256519078138L)
                .build();
        nursingPlanMapper.updateById(nursingPlan);
    }

    /**
     * 根据id删除
     *
     * @param id
     */
    @Override
    @Transactional
    public void deleteById(Long id) {
        //如果护理计划状态是启用，禁止删除
        NursingPlanVo nursingPlanVo = nursingPlanMapper.getById(id);
        if (nursingPlanVo == null) {
            //说明id数据有问题
            throw new RuntimeException("请求参数有误，操作失败!");
        }

        if (ObjectUtil.equals(nursingPlanVo.getStatus(), 1)) {
            //说明状态是启用，禁止删除
            throw new RuntimeException("护理计划状态已启用，禁止删除!");
        }

        nursingPlanMapper.deleteById(id);
        nursingProjectPlanMapper.deleteByPlanId(id);
    }

    /**
     * 查询所有护理计划
     *
     * @return
     */
    @Override
    public List<NursingPlanVo> getAll() {
        return nursingPlanMapper.getAll();
    }
}
