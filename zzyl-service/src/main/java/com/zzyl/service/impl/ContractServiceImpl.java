package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.ContractDto;
import com.zzyl.entity.Contract;
import com.zzyl.entity.Member;
import com.zzyl.mapper.ContractMapper;
import com.zzyl.service.ContractService;
import com.zzyl.service.MemberService;
import com.zzyl.utils.ThreadLocalUtil;
import com.zzyl.vo.ContractVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractMapper contractMapper;

    @Resource
    private MemberService memberService;

    /**
     * 根据id查询合同
     *
     * @param id 合同id
     * @return 合同实体类
     */
    @Override
    public ContractVo getById(Long id) {
        Contract contract = contractMapper.selectByPrimaryKey(id);
        return BeanUtil.toBean(contract, ContractVo.class);
    }

    /**
     * 更新合同
     *
     * @param contractDto 合同dto
     * @return 更新结果
     */
    @Override
    public int update(ContractDto contractDto) {
        Contract contract = new Contract();
        BeanUtils.copyProperties(contractDto, contract);
        return contractMapper.updateByPrimaryKey(contract);
    }

    /**
     * 根据id删除合同
     *
     * @param id 合同id
     * @return 删除结果
     */
    @Override
    public int deleteById(Long id) {
        return contractMapper.deleteByPrimaryKey(id);
    }

    /**
     * 分页查询合同信息
     *
     * @param pageNum    页码
     * @param pageSize   每页大小
     * @param contractNo 合同编号
     * @param elderName  老人姓名
     * @param status     合同状态
     * @param startTime  开始时间
     * @param endTime    结束时间
     * @return 分页结果
     */
    @Override
    public PageBean<ContractVo> selectByPage(Integer pageNum, Integer pageSize, String contractNo, String elderName,
                                             Integer status, LocalDateTime startTime, LocalDateTime endTime) {
        PageHelper.startPage(pageNum, pageSize);

        // 通过丙方手机号关联合同
        String phone = null;
        Long userId = ThreadLocalUtil.get();
        if (ObjectUtil.isNotEmpty(userId)) {
            Member byId = memberService.getById(userId);
            if (ObjectUtil.isNotEmpty(byId)) {
                phone = byId.getPhone();
            }
        }
        Page<Contract> page = contractMapper.selectByPage(phone, contractNo, elderName, status, startTime, endTime);
        return PageBean.of(page, ContractVo.class);
    }

    @Override
    public List<Contract> listAllContracts() {
        return contractMapper.listAllContracts();
    }

    /**
     * 批量修改合同
     *
     * @param updateList 待更新的合同数据
     * @param status     合同状态{@link com.zzyl.enums.ContractStatusEnum}
     */
    @Override
    public void updateBatchById(List<Contract> updateList, Integer status) {
        List<Long> collect = updateList.stream().map(Contract::getId).collect(Collectors.toList());
        contractMapper.batchUpdateByPrimaryKeySelective(collect, status);
    }

    @Override
    public List<ContractVo> listByMemberPhone(String phone) {
        return contractMapper.listByMemberPhone(phone);
    }

    /**
     * 根据合同状态查询合同
     *
     * @param status 合同状态{@link com.zzyl.enums.ContractStatusEnum}
     * @return 合同列表
     */
    @Override
    public List<Contract> listByStatus(Integer status) {
        return contractMapper.listByStatus(status);
    }
}


