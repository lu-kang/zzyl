package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageBean;
import com.zzyl.dto.ReservationDto;
import com.zzyl.dto.ReservationQueryDto;
import com.zzyl.entity.Reservation;
import com.zzyl.mapper.ReservationMapper;
import com.zzyl.service.ReservationService;
import com.zzyl.utils.ThreadLocalUtil;
import com.zzyl.vo.ReservationVo;
import com.zzyl.vo.TimeCountVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Slf4j
@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationMapper reservationMapper;

    /**
     * 分页查找预约（小程序端）
     */
    @Override
    public PageBean<ReservationVo> findByPage4Customer(ReservationQueryDto dto) {
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        dto.setUserId(ThreadLocalUtil.get());
        Page<Reservation> page = reservationMapper.findByPage(dto);
        return PageBean.of(page, ReservationVo.class);
    }

    /**
     * 分页查找预约（管理端）
     */
    @Override
    public PageBean<ReservationVo> findByPage4Admin(ReservationQueryDto dto) {
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        Page<Reservation> page = reservationMapper.findByPage(dto);
        return PageBean.of(page, ReservationVo.class);
    }

    /**
     * 查询用户当天取消的预约次数
     *
     * @return
     */
    @Override
    public Integer cancelledCount() {
        //根据 status、create_by、create_time
        //获取当天时间
        LocalDateTime start = LocalDateTime.now().with(LocalTime.MIN); //2024-08-11 00:00:00
        LocalDateTime end = LocalDateTime.now().with(LocalTime.MAX); //2024-08-11 23:59:59
        //从本地线程中获取用户id
        Long userId = ThreadLocalUtil.get();
        int count = reservationMapper.getCancelledCount(start, end, userId);
        return count;
    }

    /**
     * 查询时间段内的预约次数
     *
     * @param time
     * @return
     */
    @Override
    public List<TimeCountVo> countByTime(Long time) {
        //开始时间
        LocalDateTime start = LocalDateTimeUtil.of(time).with(LocalTime.MIN); //2024-08-11 00:00:00
        //结束时间
        LocalDateTime end = LocalDateTimeUtil.of(time).with(LocalTime.MAX); //2024-08-11 23:59:59

        List<TimeCountVo> timeCountVos = reservationMapper.countByTimeRange(start, end);
        return timeCountVos;
    }

    /**
     * 添加预约
     *
     * @param dto
     */
    @Override
    public void addReservation(ReservationDto dto) {
        //将DTO转换为POJO
        Reservation reservation = BeanUtil.toBean(dto, Reservation.class);
        reservation.setStatus(0);//0：待报道，1：已完成，2：取消，3：过期
        reservationMapper.insert(reservation);
    }

    /**
     * 取消预约
     *
     * @param id
     */
    @Override
    public void cancelReservation(Long id) {
        Reservation reservation = Reservation.builder()
                .id(id)
                .status(2)//0待报道 1已完成 2取消 3过期
                .build();
        reservationMapper.update(reservation);
    }
}

