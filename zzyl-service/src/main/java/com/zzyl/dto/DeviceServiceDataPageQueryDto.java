package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("设备服务调用记录分页查询请求模型")
public class DeviceServiceDataPageQueryDto {

    private Long startTime; // 服务调用记录的开始时间
    private Long endTime; // 服务调用记录的结束时间
    private String iotId; // 物联网设备id
    private Integer pageSize; // 页面大小
}
