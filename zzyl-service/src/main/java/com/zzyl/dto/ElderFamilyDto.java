package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "老人家属信息")
public class ElderFamilyDto {

    private String name; // 姓名
    private String phone; // 联系方式
    private String kinship; // 亲属关系
}
