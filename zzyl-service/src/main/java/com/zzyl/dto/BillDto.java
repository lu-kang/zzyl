package com.zzyl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "账单信息")
public class BillDto extends BaseDto {

    private String billNo; // 账单编号
    private String checkInCode; // 入住编码
    private Integer billType; // 账单类型
    private String billMonth; // 账单月份 yyyy-mm-dd
    private Long elderId; // 老人ID
    private BigDecimal billAmount; // 账单金额
    private BigDecimal payableAmount; // 应付金额
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime paymentDeadline; // 缴费截止日期
    private Integer transactionStatus; // 账单状态（0：未支付，1已支付, 2已关闭）
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime billStartTime; // 账单开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime billEndTime; // 账单结束时间
    private Integer totalDays; // 账单总天数
    private Long tradingOrderNo; // 交易号
    private String lname; // 等级名称
    private String typeName; // 房间类型名称
    private String remark; // 备注/取消原因
}
