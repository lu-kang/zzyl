package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("老人入住请求模型")
public class CheckInElderDto {

    private String name; // 姓名
    private String idCardNo; // 身份证号
    private String birthday; // 出生日期，格式：yyyy-MM-dd
    private Integer age; // 年龄
    private String sex; // 性别，0：男，1：女，2：未知
    private String phone; // 手机号
    private String address; // 家庭住址
    private String oneInchPhoto; // 一寸照片
    private String idCardNationalEmblemImg; // 身份证国徽面
    private String idCardPortraitImg; // 身份证人像面
}
