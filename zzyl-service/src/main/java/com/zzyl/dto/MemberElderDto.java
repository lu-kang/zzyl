package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("客户老人关联实体类")
public class MemberElderDto extends BaseDto {

    private Long memberId; // 客户id
    private Long elderId; // 老人id
    private String name; // 姓名
    private String idCard; // 身份证号
    private String phone; // 手机号
    private String refName; // 关系
    private String refId; // 关系Id
}


