package com.zzyl.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("我的申请DTO")
public class ApplicationsDto {

    private String code; // 申请单号
    @JsonIgnore
    private Long applicatId;
    private Integer type; // 单据类别
    private Integer status; // 状态
    private LocalDateTime startTime; // 开始时间
    private LocalDateTime endTime; // 结束时间
    private Integer pageNum; // 当前页
    private Integer pageSize; // 每页条数
}
