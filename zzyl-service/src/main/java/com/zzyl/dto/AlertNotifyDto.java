package com.zzyl.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 报警通知消息对象
 **/
@Data
@NoArgsConstructor
public class AlertNotifyDto {

    private Long id;//报警数据id
    private String accessLocation;//接入位置
    private Integer locationType;//位置类型 0：随身设备 1：固定设备
    private Integer physicalLocationType;//物理位置类型 0楼层 1房间 2床位
    private String deviceDescription;//设备位置
    private String productName;//产品名称
    private String functionName;//功能名称
    private String dataValue;//数据值
    private Integer alertDataType;//报警数据类型，0：老人异常数据，1：设备异常数据
    private Integer voiceNotifyStatus;//语音通知状态，0：关闭，1：开启
    private Integer notifyType;//报警通知类型，0：解除报警，1：报警
    private Boolean isAllConsumer = false;//是否全员通知 智能床位的报警消息是全员通知，对于护理员和固定设备维护人员不是全员通知
}
