package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("设备分页查询请求模型")
public class DevicePageQueryDto {

    private String deviceName; // 设备名称
    private String productKey; // 产品的ProductKey,物联网平台产品唯一标识
    private Integer locationType; // 位置类型 0 随身设备 1固定设备
    private Integer pageNum; // 页码
    private Integer pageSize; // 页面大小
}
