package com.zzyl.dto;

import lombok.Data;

/**
 * 消息数量映射模型
 */
@Data
public class MessageCountDto {

    private Integer readStatus; // 已读未读状态，0：未读，1：已读
    private Long count; // 数量
}
