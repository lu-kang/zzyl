package com.zzyl.dto;


import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(description = "账单分页查询参数")
public class BillPageQueryDto {

    private Integer pageSize;
    private Integer pageNum;

    //管理端请求参数
    private String billNo;//账单编号
    private String elderName;//老人姓名
    private String elderIdCard;//老人身份证号
    private Integer transactionStatus;//账单状态

    //小程序端携带的请求参数
    private Long elderId;


    private LocalDateTime startTime;
    private LocalDateTime endTime;

    private List<Long> elderIds; //sql中的查询条件
}
