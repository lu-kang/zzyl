package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "订单分页查询参数")
public class OrderPageQueryDto {

    /**
     * 小程序端、管理端携带的查询参数
     */
    private Integer pageSize;
    private Integer pageNum;
    private Integer status;

    /**
     * 管理端可能携带的查询参数
     */
    private String orderNo;
    private String elderlyName;
    private String creator;
    private Long startTime; //前端携带的是long类型毫秒值
    private Long endTime;   //前端携带的是long类型毫秒值

    //当前登录人的用户id
    private Long userId;
}
