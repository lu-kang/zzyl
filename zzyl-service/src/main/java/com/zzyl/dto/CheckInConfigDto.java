package com.zzyl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "入住配置信息")
public class CheckInConfigDto {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInStartTime; // 入住开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInEndTime; // 入住结束时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costStartTime; // 费用开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime costEndTime; // 费用结束时间

    private Long nursingLevelId; // 护理等级ID
    private String nursingLevelName; // 护理等级名称
    private Long bedId; // 床位Id
    private BigDecimal depositAmount; // 押金金额
    private BigDecimal nursingCost; // 护理费用
    private BigDecimal bedCost; // 床位费用
    private BigDecimal otherCost; // 其他费用
    private BigDecimal medicalInsurancePayment; // 医保支付
    private BigDecimal governmentSubsidy; // 政府补贴
    private Long roomId; // 房间ID
    private Long floorId; // 楼层id
    private String floorName; // 楼层名称
    private String code; // 房间编号


}
