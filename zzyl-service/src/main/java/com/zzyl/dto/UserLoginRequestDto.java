package com.zzyl.dto;

import lombok.Data;

/**
 * C端用户登录
 */
@Data
public class UserLoginRequestDto {

    private String nickName; // 昵称
    private String code; // 登录临时凭证
    private String phoneCode; // 手机号临时凭证
}
