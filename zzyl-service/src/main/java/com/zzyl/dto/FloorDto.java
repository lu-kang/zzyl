package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import lombok.Data;

@Data
public class FloorDto extends BaseDto {

    private String name; // 楼层名称
    private Integer code; // 楼层编号
}
