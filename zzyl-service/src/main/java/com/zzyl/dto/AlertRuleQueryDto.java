package com.zzyl.dto;

import lombok.Data;

@Data
public class AlertRuleQueryDto {

    private Integer pageNum;
    private Integer pageSize;
    private String alertRuleName;
    private String productKey;
    private String functionName;
}