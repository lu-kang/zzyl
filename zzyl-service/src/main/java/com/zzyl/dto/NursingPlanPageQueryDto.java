package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("护理计划分页查询请求参数对象")
public class NursingPlanPageQueryDto {

    private String name;
    private Integer pageNum;
    private Integer pageSize;
    private Integer status;
}
