package com.zzyl.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class ReservationQueryDto {

    private int pageNum;
    private int pageSize;
    private String name;
    private String phone;
    private Integer status;
    private Integer type;
    private Long userId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endTime;
}
