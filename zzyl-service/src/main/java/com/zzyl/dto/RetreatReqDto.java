package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RetreatReqDto extends BaseDto {

    private String retreatCode; // 退住编码
    private String name; // 老人姓名
    private String idCardNo; // 身份证号
    private LocalDateTime startTime; // 开始时间
    private LocalDateTime endTime; // 结束时间
    private Integer pageNum; // 当前页
    private Integer pageSize; // 每页条数
}
