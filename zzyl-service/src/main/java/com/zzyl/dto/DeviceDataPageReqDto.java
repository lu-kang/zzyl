package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("设备数据分页查询请求模型")
public class DeviceDataPageReqDto {

    private String deviceName; // 设备名称
    private String functionId; // 功能ID
    private Integer pageNum; // 页码
    private Integer pageSize; // 页面大小
    private String locationType; // 绑定位置

    private Long startTime; // 开始时间
    private Long endTime; // 结束时间

    private LocalDateTime startDate;
    private LocalDateTime endDate;
}