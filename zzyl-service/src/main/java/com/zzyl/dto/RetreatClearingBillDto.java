package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import com.zzyl.vo.retreat.DueBack;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class RetreatClearingBillDto extends BaseDto {

    private Long elderId; // 老人ID
    private LocalDateTime localDateTime; // 实际退住时间
    private BigDecimal depositDeductions; // 押金扣减金额 （元）
    private BigDecimal billDeductions; // 账单扣减金额  （元）
    private String billNo; // 账单编号
    private Long userId; // 操作人id
    private Integer day; // 退住天数
    private String remark; // 账单备注
    private String depositRemark; // 押金备注
    private List<DueBack> dueBackList;
}
