package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户信息DTO")
public class MemberDto extends BaseDto {

    private String authId; // 认证id
    private String idCardNo; // 身份证号
    private Integer idCardNoVerify; // 身份证号是否认证 1认证
    private String phone; // 手机号
    private String name; // 姓名
    private String avatar; // 头像
    private String openId; // openId
    private Integer sex; // 性别
    private String birthday; // 生日
}


