package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("床位实体类")
public class BedDto extends BaseDto {

    @ApiModelProperty("床位编号")
    private String bedNumber; // 床位编号
    private Integer bedStatus; // 床位状态: 未入住0, 已入住1
    private Long roomId; // 房间ID
    private Integer sort; // 排序号
}
