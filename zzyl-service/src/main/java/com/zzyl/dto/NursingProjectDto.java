package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 护理项目数据传输对象
 */
@Data
public class NursingProjectDto extends BaseDto {

    private String name; // 名称
    private Integer orderNo; // 排序号
    private String unit; // 单位
    private BigDecimal price; // 价格
    private String image; // 图片
    private String nursingRequirement; // 护理要求
    private Integer status; // 状态（0：禁用，1：启用）
}
