package com.zzyl.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class QueryDeviceDataDto {

    private String functionId;
    private String iotId;
    private Long startTime;
    private Long endTime;

    private LocalDateTime startDate;
    private LocalDateTime endDate;
}
