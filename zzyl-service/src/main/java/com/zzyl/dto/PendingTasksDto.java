package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("待办DTO")
public class PendingTasksDto {

    private String code; // 申请单号
    private String applicat; // 申请人
    private Long applicatId; // 申请人id
    private Integer type; // 单据类别（1：退住，2：请假，3：入住）
    private Integer status; // 状态（1：申请中，2:已完成，3:已关闭）
    private Integer applyStatus; // 申请状态
    private Date startTime; // 开始时间
    private Date endTime; // 结束时间
    private Integer pageNum; // 当前页
    private Integer pageSize; // 每页条数
    private Long assigneeId; // 操作人
    private Integer reqType; // 请求类型(0:待办，1:我的申请)
    private Integer isHandle; // 是否处理完成 (0:未处理,1:已处理)
}
