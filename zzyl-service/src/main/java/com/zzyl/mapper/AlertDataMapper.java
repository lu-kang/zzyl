package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.AlertDataPageQueryDto;
import com.zzyl.entity.AlertData;
import com.zzyl.vo.AlertDataVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AlertDataMapper {

    int deleteByPrimaryKey(Long id);

    int insert(AlertData record);

    int insertSelective(AlertData record);

    AlertData selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AlertData record);

    int updateByPrimaryKey(AlertData record);

    int batchInsert(@Param("list") List<AlertData> list);

    /*Page<AlertDataVo> adminPageQuery(@Param("id") Long id,
                                     @Param("status") Integer status,
                                     @Param("deviceName") String deviceName,
                                     @Param("userId") Long userId,
                                     @Param("minCreateTime") LocalDateTime minCreateTime,
                                     @Param("maxCreateTime") LocalDateTime maxCreateTime);*/
    Page<AlertDataVo> adminPageQuery(AlertDataPageQueryDto dto);

    void handleAlertData(AlertData alertData);

    /**
     * 多条件查询
     *
     * @param alertData
     * @return
     */
    List<AlertData> selectByAlertData(AlertData alertData);

}