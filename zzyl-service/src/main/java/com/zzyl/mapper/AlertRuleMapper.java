package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.AlertRuleQueryDto;
import com.zzyl.entity.AlertRule;
import com.zzyl.vo.AlertRuleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AlertRuleMapper {

    /**
     * 删除报警规则
     *
     * @param id 报警规则id
     * @return 删除数量
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 插入报警规则
     *
     * @param alertRule 报警规则
     * @return 新增数量
     */
    int insert(AlertRule alertRule);

    /**
     * 根据主键查询报警规则
     *
     * @param id 报警规则id
     * @return 报警规则
     */
    AlertRule selectByPrimaryKey(Long id);

    /**
     * 条件查询报警规则
     *
     * @param productKey 所属产品key
     * @param iotId      设备id
     * @param functionId 功能标识符
     * @return 报警规则列表
     */
    List<AlertRule> selectByFunctionId(@Param("productKey") String productKey,
                                       @Param("iotId") String iotId,
                                       @Param("functionId") String functionId);

    /**
     * 根据主键更新报警规则
     *
     * @param alertRule 报警规则
     * @return 更新数量
     */
    int updateByPrimaryKeySelective(AlertRule alertRule);

    /**
     * 分页查询报警规则
     *
     * @return 报警规则分页结果
     */
    Page<AlertRuleVo> page(AlertRuleQueryDto dto);

    /**
     * 启用或禁用
     *
     * @param id     ID
     * @param status 状态，0：禁用，1：启用
     */
    void updateStatus(@Param("id") Long id,
                      @Param("status") Integer status);

    /**
     * 查询所有规则
     *
     * @return
     */
    List<AlertRule> selectAll();

}