package com.zzyl.mapper;

import com.zzyl.entity.Bed;
import com.zzyl.vo.BedVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BedMapper {

    /**
     * 根据房间id获取床位信息列表
     *
     * @param roomId 房间id
     * @return 床位对象列表
     */
    List<BedVo> getBedsByRoomId(@Param("roomId") Long roomId);

    /**
     * 添加
     *
     * @param bed
     */
    void insert(Bed bed);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Select("select * from bed where id=#{id}")
    Bed getById(Long id);

    /**
     * 根据id更新
     *
     * @param bed
     */
    void updateById(Bed bed);

    /**
     * 根据id删除
     *
     * @param id
     */
    @Delete("delete from bed where id=#{id}")
    void deleteById(Long id);
}

