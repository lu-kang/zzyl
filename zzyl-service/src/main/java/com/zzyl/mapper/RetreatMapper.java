package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.entity.Retreat;
import com.zzyl.vo.RetreatPageQueryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;

@Mapper
public interface RetreatMapper {

    /**
     * 新增退住信息
     *
     * @param retreat 退住信息
     */
    void insert(Retreat retreat);


    void update(Retreat retreat);

    @Select("select * from retreat where elder_id = #{elderId}")
    Retreat selectByElderId(@Param("elderId") Long elderId);

    /**
     * 分页查询
     *
     * @param elderName        老人姓名，模糊查询
     * @param elderIdCardNo    身份证号，精确查询
     * @param retreatStartTime 退住开始时间
     * @param retreatEndTime   退住结束时间
     * @return 分页结果
     */
    Page<RetreatPageQueryVo> pageQuery(@Param("elderName") String elderName,
                                       @Param("elderIdCardNo") String elderIdCardNo,
                                       @Param("retreatStartTime") LocalDateTime retreatStartTime,
                                       @Param("retreatEndTime") LocalDateTime retreatEndTime);

    /**
     * 根据id查询
     *
     * @param id 退住id
     * @return 退住信息
     */
    @Select("select * from retreat where id = #{id}")
    Retreat selectById(@Param("id") Long id);
}
