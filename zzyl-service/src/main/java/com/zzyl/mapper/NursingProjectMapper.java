package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.NursingProjectPageQueryDto;
import com.zzyl.entity.NursingProject;
import com.zzyl.vo.NursingProjectVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface NursingProjectMapper {

    /**
     * 分页条件查询护理项目
     *
     * @param dto
     * @return
     */
    Page<NursingProjectVo> pageQuery(NursingProjectPageQueryDto dto);

    /**
     * 添加
     *
     * @param nursingProject
     */
    void insert(NursingProject nursingProject);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Select("select * from nursing_project where id=#{id}")
    NursingProject getById(Long id);

    /**
     * 根据id修改护理项目
     *
     * @param nursingProject
     */
    void update(NursingProject nursingProject);

    /**
     * 根据id删除
     *
     * @param id
     */
    @Delete("delete from nursing_project where id=#{id}")
    void deleteById(Long id);

    /**
     * 查询所有护理项目
     *
     * @return
     */
    @Select("select * from nursing_project where status=1")
    List<NursingProject> selectAll();
}