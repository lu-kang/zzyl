package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.ReservationQueryDto;
import com.zzyl.entity.Reservation;
import com.zzyl.vo.TimeCountVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface ReservationMapper {

    int insert(Reservation reservation);

    int update(Reservation reservation);

    int deleteById(Long id);

    Reservation findById(Long id);

    Page<Reservation> findByPage(ReservationQueryDto dto);

    Integer getByTimeAndUserId(@Param("time") LocalDateTime time,
                               @Param("createBy") Long updateBy);

    List<TimeCountVo> countByTimeRange(@Param("startTime") LocalDateTime startTime,
                                       @Param("endTime") LocalDateTime endTime);

    int getCancelledCount(@Param("startTime") LocalDateTime startTime,
                          @Param("endTime") LocalDateTime endTime,
                          @Param("updateBy") Long updateBy);

    List<Reservation> getTimeoutReservation(LocalDateTime time);
}
