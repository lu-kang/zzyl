package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.NursingLevelPageQueryDto;
import com.zzyl.entity.NursingLevel;
import com.zzyl.vo.NursingLevelVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface NursingLevelMapper {

    /**
     * 根据护理计划id查询
     *
     * @param planId
     * @return
     */
    @Select("select * from nursing_level where plan_id=#{planId}")
    List<NursingLevel> getByPlanId(Long planId);


    /**
     * 条件查询
     *
     * @param dto
     * @return
     */
    Page<NursingLevel> pageQuery(NursingLevelPageQueryDto dto);

    /**
     * 添加
     *
     * @param level
     */
    void insert(NursingLevel level);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Select("select * from nursing_level where id=#{id}")
    NursingLevel getById(Long id);

    /**
     * 修改
     *
     * @param level
     */
    void update(NursingLevel level);

    /**
     * 根据id删除
     *
     * @param id
     */
    @Delete("delete from nursing_level where id=#{id}")
    void deleteById(Long id);

    /**
     * 查询所有
     *
     * @return
     */
    @Select("select * from nursing_level where status=1")
    List<NursingLevelVo> getAll();
}
