package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.DeviceDataPageReqDto;
import com.zzyl.dto.QueryDeviceDataDto;
import com.zzyl.entity.DeviceData;
import com.zzyl.vo.DeviceDataGraphVo;
import com.zzyl.vo.DeviceDataVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DeviceDataMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DeviceData record);

    int insertSelective(DeviceData record);

    DeviceData selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DeviceData record);

    int updateByPrimaryKey(DeviceData record);

    int batchInsert(@Param("list") List<DeviceData> list);

    Page<DeviceDataVo> selectByPage(DeviceDataPageReqDto deviceDataPageReqDto);

    List<DeviceDataGraphVo> queryDeviceDataListByDay(QueryDeviceDataDto dto);

    List<DeviceDataGraphVo> queryDeviceDataListByWeek(QueryDeviceDataDto dto);

    @Delete("delete from device_data where alarm_time  <  DATE_SUB(NOW(), interval 30 day)")
    void clearDeviceDataJob();

}