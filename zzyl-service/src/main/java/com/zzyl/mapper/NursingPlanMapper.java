package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.NursingPlanPageQueryDto;
import com.zzyl.entity.NursingPlan;
import com.zzyl.vo.NursingPlanVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface NursingPlanMapper {

    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    Page<NursingPlanVo> pageQuery(NursingPlanPageQueryDto dto);

    /**
     * 添加
     *
     * @param nursingPlan
     */
    void insert(NursingPlan nursingPlan);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Select("select * from nursing_plan where id=#{id}")
    NursingPlanVo getById(Long id);

    /**
     * 更新
     *
     * @param nursingPlan
     */
    void updateById(NursingPlan nursingPlan);

    /**
     * 根据id删除
     *
     * @param id
     */
    @Delete("delete from nursing_plan where id=#{id}")
    void deleteById(Long id);

    /**
     * 查询所有护理计划
     *
     * @return
     */
    @Select("select * from nursing_plan where status=1")
    List<NursingPlanVo> getAll();

}
