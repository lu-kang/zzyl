package com.zzyl.mapper;

import com.zzyl.entity.NursingProjectPlan;
import com.zzyl.vo.NursingProjectPlanVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NursingProjectPlanMapper {

    /**
     * 添加
     *
     * @param projectPlan
     */
    void insert(NursingProjectPlan projectPlan);

    /**
     * 根据护理计划id查询
     *
     * @param planId
     * @return
     */
    List<NursingProjectPlanVo> getByPlanId(Long planId);

    /**
     * 根据护理计划id删除
     *
     * @param planId
     */
    @Delete("delete from nursing_project_plan where plan_id=#{planId}")
    void deleteByPlanId(Long planId);
}
