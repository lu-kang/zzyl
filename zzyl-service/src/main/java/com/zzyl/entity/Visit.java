package com.zzyl.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Visit extends BaseEntity {

    private String name; // 来访人
    private String mobile; // 来访人手机号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time; // 来访时间
    private String visitor; // 探访人
    private Integer type; // 来访类型，0：参观来访，1：探访来访
    private Integer status; // 来访状态，0：待报道，1：已完成，2：取消，3：过期
}
