package com.zzyl.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseEntity;
import com.zzyl.vo.*;
import com.zzyl.vo.retreat.ElderVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Order extends BaseEntity {

    private Long tradingOrderNo; // 订单号
    private Integer paymentStatus; // 支付状态
    private BigDecimal amount; // 订单金额
    private BigDecimal refund; // 退款金额
    private String isRefund; // 是否退款
    private Long memberId; // 下单会员ID
    private Long projectId; // 项目ID
    private Long elderId; // 老人ID
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime estimatedArrivalTime; // 预计到达时间
    private String orderNo; // 备注
    private String reason; // 原因
    private Integer status; // 订单状态
    private BedVo bedVo;
    private ElderVo elderVo;
    private MemberVo memberVo;
    private NursingProjectVo nursingProjectVo;
    private TradingVo tradingVo;//支付信息
    private RefundRecordVo refundRecordVo;//退款记录
    private NursingTaskVo nursingTaskVo;//执行记录
    private Integer createType;
}
