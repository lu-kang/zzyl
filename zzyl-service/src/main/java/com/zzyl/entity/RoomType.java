package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RoomType extends BaseEntity {

    private String name; // 房型名称
    private Integer bedCount; // 床位数量
    private BigDecimal price; // 床位费用
    private String introduction; // 介绍
    private String photo; // 照片
    private String typeName; // 类型名称
    private Integer status; // 状态，0：禁用，1：启用
}
