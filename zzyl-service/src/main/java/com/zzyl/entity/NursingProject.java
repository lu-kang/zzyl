package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

/**
 * nursing_project 实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NursingProject extends BaseEntity {

    private String name; // 名称
    private Integer orderNo; // 排序号
    private String unit; // 单位
    private BigDecimal price; // 价格
    private String image; // 图片
    private String nursingRequirement; // 护理要求
    private Integer status; // 状态（0：禁用，1：启用）
}
