package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import com.zzyl.dto.ElderDto;
import com.zzyl.vo.BedVo;
import com.zzyl.vo.CheckInConfigVo;
import com.zzyl.vo.NursingLevelVo;
import com.zzyl.vo.RoomVo;
import com.zzyl.vo.retreat.ElderVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "入住实体类")
public class CheckIn extends BaseEntity {

    private String checkInCode; // 入住单号
    private String title; // 入住标题
    private ElderDto elderDto; // 老人
    private String otherApplyInfo; // 其他申请信息
    private Long elderId; // 老人id
    private LocalDateTime checkInTime; // 入住时间
    private String remark; // 备注
    private String applicat; // 申请人
    private String deptNo; // 申请人部门编号
    private Long applicatId; // 申请人id
    private LocalDateTime createTime; // 申请时间
    private Integer status; // 状态
    private ElderVo elderVo;
    private RoomVo roomVo;
    private CheckInConfigVo checkInConfigVo;
    private NursingLevelVo nursingLevelVo;
    private BedVo bedVo;
}
