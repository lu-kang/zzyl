package com.zzyl.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseEntity;
import com.zzyl.vo.BedVo;
import com.zzyl.vo.CheckInConfigVo;
import com.zzyl.vo.RefundRecordVo;
import com.zzyl.vo.TradingVo;
import com.zzyl.vo.retreat.ElderVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


/**
 * 账单实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Bill extends BaseEntity {

    private Long id; // 账单id
    private String billNo; // 账单编号
    private String checkInCode; // 入住编码
    private Integer billType; // 账单类型
    private String billMonth; // 账单月份
    private Long elderId; // 老人id
    private BigDecimal billAmount; // 账单金额
    private BigDecimal payableAmount; // 应付金额
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime paymentDeadline; // 缴费截止日期
    private Integer transactionStatus; // 交易状态
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime billStartTime; // 账单开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime billEndTime; // 账单结束时间
    private Integer totalDays; // 账单总天数
    private BigDecimal currentCost; // 本期应付
    private BigDecimal depositAmount; // 押金金额
    private BigDecimal prepaidAmount; // 预付款支付金额
    private Long tradingOrderNo;
    private String lname; // 等级名称
    private String typeName; // 房间类型名称
    private ElderVo elderVo;
    private BigDecimal total; // 总欠费金额
    private List<TradingVo> tradingVo;
    private RefundRecordVo refundRecordVo;
    private CheckInConfigVo checkInConfigVo;
    private BedVo bedVo;
}
