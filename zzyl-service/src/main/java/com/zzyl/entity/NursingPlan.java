package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@ApiModel("护理计划")
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NursingPlan extends BaseEntity {

    private Integer sortNo; // 排序号
    private String planName; // 计划名称
    private Integer status; // 状态（0：禁用，1：启用）
}
