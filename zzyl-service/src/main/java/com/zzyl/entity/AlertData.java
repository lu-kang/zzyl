package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

/**
 * 报警数据
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AlertData extends BaseEntity {

    private static final long serialVersionUID = -4958513062287152699L;

    private String iotId; // 物联网设备id
    private String deviceName; // 设备名称
    private String nickname; // 设备备注名称
    private String productKey; // 所属产品key
    private String productName; // 产品名称
    private String functionId; // 功能标识符
    private String accessLocation; // 接入位置
    private Integer locationType; // 位置类型 0：随身设备 1：固定设备
    private Integer physicalLocationType; // 物理位置类型 0楼层 1房间 2床位
    private String deviceDescription; // 位置备注
    private String dataValue; // 数据值
    private LocalDateTime alarmTime; // 数据上报时间
    private Long alertRuleId; // 报警规则id
    private String alertReason; // 报警原因，格式：功能名称+运算符+阈值+持续周期+聚合周期
    private String processingResult; // 处理结果
    private Long processorId; // 处理人id
    private String processorName; // 处理人名称
    private LocalDateTime processingTime; // 处理时间
    private Integer type; // 报警数据类型，0：老人异常数据，1：设备异常数据
    private Integer status; // 状态，0：待处理，1：已处理
    private Long userId; // 接收者id
}