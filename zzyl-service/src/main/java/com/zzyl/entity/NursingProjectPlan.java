package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "NursingProjectPlan对象", description = "护理项目计划实体类")
public class NursingProjectPlan extends BaseEntity {

    private Long planId; // 计划id
    private Long projectId; // 项目id
    private String executeTime; // 计划执行时间
    private Integer executeCycle; // 执行周期
    private Integer executeFrequency; // 执行频次
}
