package com.zzyl.constant;

public class CacheConstants {

    public static final String RESOURCE_LIST = "resource:list";
    public static final String RESOURCE_TREE = "resource:tree";

    public static final String USER_ACCESS_URLS = "user:access:urls:";

    public static final String IOT_ALL_PRODUCT = "iot:all:product";
    public static final String IOT_DEVICE_LAST_DATA = "iot:device:last:data";

    //报警规则连续触发次数，缓存前缀
    public static final String ALERT_TRIGGER_COUNT = "alert:trigger:count:";
    //报警规则沉默周期，缓存前缀
    public static final String ALERT_SILENT_CYCLE = "alert:silent:cycle:";
}
