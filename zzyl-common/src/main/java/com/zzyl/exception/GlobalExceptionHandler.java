package com.zzyl.exception;

import com.zzyl.base.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器
 */
@Slf4j
@Component
@RestControllerAdvice
public class GlobalExceptionHandler {

    //处理自定义异常（可选）
    @ExceptionHandler(BaseException.class)
    public ResponseResult<String> handlerBaseError(BaseException ex) {
        //打印异常到控制台
        ex.printStackTrace();
        //响应数据
        return ResponseResult.error(ex.getBasicEnum());
    }


    //处理运行期异常 Runtime
    @ExceptionHandler(RuntimeException.class)
    public ResponseResult<String> handlerRuntimeError(RuntimeException ex) {
        //打印异常到控制台
        ex.printStackTrace();
        //响应数据
        return ResponseResult.error(ex.getMessage());
    }

    //默认处理
    @ExceptionHandler(Exception.class)
    public ResponseResult<String> handlerDefaultError(Exception ex) {
        //打印异常到控制台
        ex.printStackTrace();
        //响应数据
        return ResponseResult.error(ex.getMessage());
    }

}
