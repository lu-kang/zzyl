package com.zzyl.config;

import com.aliyun.iot20180120.Client;
import com.aliyun.teaopenapi.models.Config;
import com.zzyl.properties.AliyunIotProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 和IOT相关的配置
 */
@Configuration
public class AliyunIotConfig {

    //将Iot平台SDK中最重要的对象放入容器  Client
    @Bean
    public Client client(AliyunIotProperties aliyunIotProperties) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(aliyunIotProperties.getAccessKeyId())
                // 您的AccessKey Secret
                .setAccessKeySecret(aliyunIotProperties.getAccessKeySecret());
        // 访问的域名
        config.setRegionId(aliyunIotProperties.getRegionId());

        return new Client(config);
    }

}
