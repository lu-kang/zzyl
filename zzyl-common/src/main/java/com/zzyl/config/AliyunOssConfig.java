package com.zzyl.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.zzyl.properties.AliyunOssProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里云Oss配置类
 */
@Slf4j
@Configuration
public class AliyunOssConfig {

    @Autowired
    private AliyunOssProperties aliyunOssProperties;

    @Bean
    public OSS ossClient() {
        return new OSSClientBuilder().build(aliyunOssProperties.getEndpoint(),
                aliyunOssProperties.getAccessKeyId(),
                aliyunOssProperties.getAccessKeySecret());
    }
}
