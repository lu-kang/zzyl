package com.zzyl.config;

import com.zzyl.intercept.JwtTokenInterceptor;
import com.zzyl.intercept.MobileInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WebMvc高级配置类
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    MobileInterceptor mobileInterceptor;
    @Autowired
    JwtTokenInterceptor jwtTokenInterceptor;

    /**
     * 注册拦截器
     *
     * @param registry
     */
    public void addInterceptors(InterceptorRegistry registry) {

        //添加移动端拦截器
        registry.addInterceptor(mobileInterceptor)
                //拦截的路径：只拦截小程序端的请求，以customer开头
                .addPathPatterns("/customer/**")
                //排除的路径
                .excludePathPatterns("/customer/user/login");

        //添加管理端拦截器
        registry.addInterceptor(jwtTokenInterceptor)
                .addPathPatterns("/**")
                //排除的路径：小程序端所有的请求、后台管理端登录地址、swagger接口文档
                .excludePathPatterns("/customer/**", "/security/login", "/common/upload",
                        "/swagger-resources/**", "/webjars/**", "/swagger-ui.html/**",
                        "/doc.html", "/v2/api-docs");

    }

}
