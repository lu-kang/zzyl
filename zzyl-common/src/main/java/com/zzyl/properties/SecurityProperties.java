package com.zzyl.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * Security配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "zzyl.framework.security")
public class SecurityProperties {

    private String defaultPassword;//默认密码
    private List<String> ignoreUrl;//忽略列表
}
