package com.zzyl.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里云Oss配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "zzyl.aliyun.oss")
public class AliyunOssProperties {

    private String endpoint;//域名站点
    private String accessKeyId;//秘钥Id
    private String accessKeySecret;//秘钥
    private String bucketName;//桶名称
}

