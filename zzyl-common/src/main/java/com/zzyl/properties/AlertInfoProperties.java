package com.zzyl.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 报警配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "zzyl.framework.alert")
public class AlertInfoProperties {

    private String deviceMaintainerRole; //设备维护人员的角色名称
    private String managerRole; //超级管理员的角色名称
}
