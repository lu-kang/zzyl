package com.zzyl.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 微信配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "zzyl.wechat")
public class WeChatProperties {

    private String appid; //小程序的appid
    private String secret; //小程序的密钥

    /////////////////////////////////////////////////////////// 微信支付相关设置

    private String mchId; //商户号
    private String mchSerialNo; //商户API证书的证书序列号
    private String privateKey; //商户私钥文件
    private String apiV3Key; //证书解密的密钥
    private String notifyUrl; //支付成功的回调地址
}
