package com.zzyl.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

/**
 * Swagger配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "zzyl.framework.swagger")
public class SwaggerProperties implements Serializable {

    private String title;//项目名称
    private String description;//具体描述
    private String contactName;//组织名称
    private String contactUrl;//联系网址
    private String contactEmail;//联系邮箱
}
