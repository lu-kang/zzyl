package com.zzyl.base;

import com.zzyl.enums.BasicEnum;
import com.zzyl.utils.HttpStatus;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@ApiModel(value = "统一返回结果", description = "统一返回结果")
public class ResponseResult<T> implements Serializable {

    private int code;//状态码
    private String msg;//状态信息
    private T data;//返回结果

    public ResponseResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 返回成功消息
     */
    public static <T> ResponseResult<T> success() {
        return new ResponseResult<>(HttpStatus.SUCCESS, "操作成功");
    }

    /**
     * 返回成功数据
     */
    public static <T> ResponseResult<T> success(T data) {
        return new ResponseResult<>(HttpStatus.SUCCESS, "操作成功", data);

    }


    /**
     * 返回错误消息
     */
    public static <T> ResponseResult<T> error(String msg) {
        return new ResponseResult<>(HttpStatus.ERROR, msg);
    }

    /**
     * 返回错误消息
     */
    public static <T> ResponseResult<T> error(BasicEnum basicEnum) {
        return new ResponseResult<>(basicEnum.getCode(), basicEnum.getMsg());
    }
}
