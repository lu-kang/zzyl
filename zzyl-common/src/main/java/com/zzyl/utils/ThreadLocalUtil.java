package com.zzyl.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * 本地线程维护工具
 */
@Slf4j
public class ThreadLocalUtil {

    //本地线程
    private static final ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 设置线程数据
     *
     * @param userId
     */
    public static void set(Long userId) {
        threadLocal.set(userId);
    }

    /**
     * 获取线程数据
     *
     * @return
     */
    public static Long get() {
        return threadLocal.get();
    }

    /**
     * 移除线程数据
     */
    public static void remove() {
        threadLocal.remove();
    }
}
