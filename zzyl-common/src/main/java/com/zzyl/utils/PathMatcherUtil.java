package com.zzyl.utils;

import cn.hutool.core.collection.CollUtil;
import org.springframework.util.AntPathMatcher;

import java.util.List;

/**
 * 路径匹配工具
 */
public class PathMatcherUtil {

    private static final AntPathMatcher antPathMatcher = new AntPathMatcher();

    /**
     * 判断path是否符合规则
     *
     * @param patterns
     * @param path
     * @return
     */
    public static boolean match(List<String> patterns, String path) {
        if (CollUtil.isEmpty(patterns)) {
            return false;
        }
        for (String pattern : patterns) {
            if (antPathMatcher.match(pattern, path)) {
                return true;
            }
        }
        return false;
    }
}
