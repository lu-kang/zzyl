package com.zzyl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@SpringBootApplication
@EnableScheduling //开启定时任务
@EnableCaching //开启缓存功能
public class ZzylApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZzylApplication.class, args);
        log.info("项目启动成功，接口文档地址：http://127.0.0.1:9995/doc.html");
    }

}
