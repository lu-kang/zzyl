package com.zzyl.controller.manager;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.service.NursingTaskService;
import com.zzyl.vo.NursingTaskVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/nursingTask")
@Api(tags = "护理任务管理")
public class NursingTaskController {

    @Resource
    private NursingTaskService nursingTaskService;

    @GetMapping
    @ApiOperation("根据ID获取护理任务")
    ResponseResult<NursingTaskVo> getById(@RequestParam("taskId") Long taskId) {
        return ResponseResult.success(nursingTaskService.selectByPrimaryKey(taskId));
    }

    @GetMapping("page")
    @ApiOperation("分页查询任务")
    ResponseResult<PageBean<NursingTaskVo>> getTasksByPage(@RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                           @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                                                           String elderName,
                                                           Long nurseId,
                                                           Long projectId,
                                                           Long startTime,
                                                           Long endTime,
                                                           Integer status) {
        PageBean<NursingTaskVo> tasksByPage = nursingTaskService.getTasksByPage(pageNum, pageSize, elderName, nurseId, projectId,
                ObjectUtil.isEmpty(startTime) ? null : LocalDateTimeUtil.of(startTime),
                ObjectUtil.isEmpty(endTime) ? null : LocalDateTimeUtil.of(endTime),
                status);
        return ResponseResult.success(tasksByPage);
    }


}
