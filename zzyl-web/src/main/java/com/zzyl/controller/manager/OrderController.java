package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.OrderPageQueryDto;
import com.zzyl.service.OrderService;
import com.zzyl.vo.OrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = "订单管理相关接口")
@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @ApiOperation("分页条件查询订单")
    @GetMapping("/search")
    public ResponseResult<PageBean<OrderVo>> search(OrderPageQueryDto dto) {
        PageBean<OrderVo> pageBean = orderService.pageQuery4Admin(dto);
        return ResponseResult.success(pageBean);
    }

    @ApiOperation("根据id查询")
    @GetMapping
    public ResponseResult<OrderVo> getOrderById(@RequestParam("orderId") Long orderId) {
        OrderVo orderVo = orderService.getOrderById(orderId);
        return ResponseResult.success(orderVo);
    }
}




