package com.zzyl.controller.manager;


import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.NursingProjectDto;
import com.zzyl.dto.NursingProjectPageQueryDto;
import com.zzyl.service.NursingProjectService;
import com.zzyl.vo.NursingProjectVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/nursing_project")
@Api(tags = "护理项目管理口")
public class NursingProjectController {

    @Autowired
    NursingProjectService nursingProjectService;

    @ApiOperation("分页条件查询护理项目")
    @GetMapping
    public ResponseResult<PageBean<NursingProjectVo>> pageQuery(NursingProjectPageQueryDto dto) {
        log.info("分页条件查询护理项目:{}", dto);
        PageBean<NursingProjectVo> pageBean = nursingProjectService.pageQuery(dto);
        return ResponseResult.success(pageBean);
    }


    @ApiOperation("新增护理项目")
    @PostMapping
    public ResponseResult<Void> addNursingProject(@RequestBody NursingProjectDto dto) {
        log.info("新增护理项目:{}", dto);
        nursingProjectService.addNursingProject(dto);
        return ResponseResult.success();
    }


    @ApiOperation("根据id查询护理项目")
    @GetMapping("/{id}")
    public ResponseResult<NursingProjectVo> getById(@PathVariable Long id) {
        log.info("根据id查询护理项目:{}", id);
        NursingProjectVo nursingProjectVo = nursingProjectService.getById(id);
        return ResponseResult.success(nursingProjectVo);
    }


    @ApiOperation("修改护理项目")
    @PutMapping
    public ResponseResult<Void> updateNursingProject(@RequestBody NursingProjectDto dto) {
        log.info("修改护理项目:{}", dto);
        nursingProjectService.updateNursingProject(dto);
        return ResponseResult.success();

    }


    @ApiOperation("启用、禁用护理项目")
    @PutMapping("/{id}/status/{status}")
    public ResponseResult<Void> enableOrDisable(@PathVariable Long id,
                                                @PathVariable Integer status) {
        log.info("启用、禁用护理项目:{} {}", id, status);
        nursingProjectService.enableOrDisable(id, status);
        return ResponseResult.success();
    }

    @ApiOperation("删除护理项目")
    @DeleteMapping("/{id}")
    public ResponseResult<Void> deleteById(@PathVariable Long id) {
        log.info("删除护理项目:{}", id);
        nursingProjectService.deleteById(id);
        return ResponseResult.success();
    }

    @ApiOperation("查询所有护理项目")
    @GetMapping("/all")
    public ResponseResult<List<NursingProjectVo>> getAllNursingProject() {
        log.info("查询所有护理项目");
        List<NursingProjectVo> list = nursingProjectService.getAllNursingProject();
        return ResponseResult.success(list);
    }

}
