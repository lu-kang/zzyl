package com.zzyl.controller.manager;

import com.zzyl.base.ResponseResult;
import com.zzyl.dto.FloorDto;
import com.zzyl.service.FloorService;
import com.zzyl.vo.FloorVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/floor")
@Api(tags = "楼层管理")
public class FloorController {

    @Autowired
    private FloorService floorService;

    @ApiOperation("查询所有楼层（智能设备）")
    @GetMapping("/getAllFloorsWithDevice")
    public ResponseResult<List<FloorVo>> getAllFloorsWithDevice() {
        log.info("查询所有楼层（智能设备）");
        List<FloorVo> allFloors = floorService.getAllFloorsWithDevice();
        return ResponseResult.success(allFloors);
    }

    /////////////////////////////////////////////////////////////////////

    @PostMapping("/add")
    @ApiOperation(value = "添加楼层", notes = "传入楼层信息，返回添加结果")
    public ResponseResult<Void> addFloor(@RequestBody FloorDto floorDto) {
        floorService.addFloor(floorDto);
        return ResponseResult.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除楼层", notes = "根据楼层id删除指定楼层，返回删除结果")
    public ResponseResult<Void> deleteFloor(@PathVariable Long id) {
        floorService.deleteFloor(id);
        return ResponseResult.success();
    }

    @PutMapping("/update")
    @ApiOperation(value = "更新楼层", notes = "传入更新的楼层信息，返回更新结果")
    public ResponseResult<Void> updateFloor(@RequestBody FloorDto floorDto) {
        floorService.updateFloor(floorDto);
        return ResponseResult.success();
    }

    @GetMapping("/get/{id}")
    @ApiOperation(value = "获取楼层", notes = "根据楼层id获取指定楼层，返回楼层信息")
    public ResponseResult<FloorVo> getFloor(@PathVariable Long id) {
        return ResponseResult.success(floorService.getFloor(id));
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "获取所有楼层", notes = "无需参数，获取所有楼层，返回楼层信息列表")
    public ResponseResult<List<FloorVo>> getAllFloors() {
        return ResponseResult.success(floorService.getAllFloors());
    }

    @GetMapping("/getAllFloorsWithNur")
    @ApiOperation(value = "获取所有楼层-包含负责老人", notes = "无需参数，获取所有楼层，返回楼层信息列表")
    public ResponseResult<List<FloorVo>> getAllFloorsWithNur() {
        return ResponseResult.success(floorService.selectAllByNur());
    }

    @GetMapping("/getAllWithRoomAndBed")
    @ApiOperation(value = "获取所有楼层-包含房间和床位", notes = "无需参数，获取所有楼层，返回楼层信息列表")
    public ResponseResult<List<FloorVo>> getAllWithRoomAndBed() {
        return ResponseResult.success(floorService.getAllWithRoomAndBed());
    }

    @GetMapping("/getRoomAndBedByBedStatus/{status}")
    @ApiOperation(value = "根据床位状态查询获取所有楼层数据")
    public ResponseResult<List<FloorVo>> getRoomAndBedByBedStatus(@PathVariable("status") Integer status) {
        return ResponseResult.success(floorService.getRoomAndBedByBedStatus(status));
    }

}
