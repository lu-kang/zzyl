package com.zzyl.controller.manager;

import com.aliyun.iot20180120.models.DeleteDeviceRequest;
import com.aliyun.iot20180120.models.QueryDevicePropertyStatusRequest;
import com.aliyun.iot20180120.models.QueryThingModelPublishedRequest;
import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.DeviceDto;
import com.zzyl.dto.DevicePageQueryDto;
import com.zzyl.service.DeviceService;
import com.zzyl.vo.DevicePropertyStatusVo;
import com.zzyl.vo.DeviceVo;
import com.zzyl.vo.ProductVo;
import com.zzyl.vo.QueryThingModelVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@Api(tags = "Iot设备管理")
@RequestMapping("/iot")
@RequiredArgsConstructor
public class DeviceController {

    final DeviceService deviceService;

    @ApiOperation("产品列表数据同步")
    @PostMapping("/syncProductList")
    public ResponseResult<Void> syncProductList() {
        log.info("产品列表数据同步");
        deviceService.syncProductList2Redis();
        return ResponseResult.success();
    }

    @ApiOperation("查询所有产品列表")
    @GetMapping("/allProduct")
    public ResponseResult<List<ProductVo>> allProduct() {
        log.info("查询所有产品列表");
        List<ProductVo> productVoList = deviceService.allProduct();
        return ResponseResult.success(productVoList);
    }

    @ApiOperation("注册设备")
    @PostMapping("/RegisterDevice")
    public ResponseResult<Void> registerDevice(@RequestBody DeviceDto dto) {
        log.info("注册设备:{}", dto);
        deviceService.registerDevice(dto);
        return ResponseResult.success();
    }


    @ApiOperation("分页查询设备")
    @GetMapping("/pageQueryDevice")
    public ResponseResult<PageBean<DeviceVo>> pageQuery(DevicePageQueryDto dto) {
        log.info("分页查询设备:{}", dto);
        PageBean<DeviceVo> pageBean = deviceService.pageQuery(dto);
        return ResponseResult.success(pageBean);
    }

    @ApiOperation("查询设备详情")
    @PostMapping("/QueryDeviceDetail")
    public ResponseResult<DeviceVo> queryDeviceDetail(@RequestBody DeviceDto dto) {
        log.info("查询设备详情:{}", dto);
        DeviceVo deviceVo = deviceService.queryDeviceDetail(dto);
        return ResponseResult.success(deviceVo);
    }

    //============================================================== 课外作业

    @PostMapping("/QueryDevicePropertyStatus")
    @ApiOperation(value = "查询指定设备的物模型运行状态")
    public ResponseResult<DevicePropertyStatusVo> queryDevicePropertyStatus(@RequestBody QueryDevicePropertyStatusRequest request) {
        DevicePropertyStatusVo vo = deviceService.queryDevicePropertyStatus(request);
        return ResponseResult.success(vo);
    }

    @PostMapping("/UpdateDevice")
    @ApiOperation(value = "修改设备备注名称")
    public ResponseResult<Void> updateDevice(@RequestBody DeviceDto deviceDto) {
        deviceService.updateDevice(deviceDto);
        return ResponseResult.success();
    }

    @DeleteMapping("/DeleteDevice")
    @ApiOperation(value = "删除设备")
    public ResponseResult<Void> deleteDevice(@RequestBody DeleteDeviceRequest request) {
        deviceService.deleteDevice(request);
        return ResponseResult.success();
    }

    @PostMapping("/QueryThingModelPublished")
    @ApiOperation(value = "查看指定产品的物模型数据")
    public ResponseResult<QueryThingModelVo> queryThingModelPublished(@RequestBody QueryThingModelPublishedRequest request) {
        QueryThingModelVo queryThingModelVo = deviceService.queryThingModelPublished(request);
        return ResponseResult.success(queryThingModelVo);
    }
}
