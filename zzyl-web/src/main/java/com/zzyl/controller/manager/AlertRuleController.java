package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.AlertRuleDto;
import com.zzyl.dto.AlertRuleQueryDto;
import com.zzyl.service.AlertRuleService;
import com.zzyl.vo.AlertRuleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/alert-rule")
@Api(tags = "报警规则相关接口")
@RequiredArgsConstructor
public class AlertRuleController {

    final AlertRuleService alertRuleService;

    @PostMapping("/create")
    @ApiOperation(value = "新增报警规则")
    public ResponseResult<Void> createRule(@RequestBody AlertRuleDto alertRuleDto) {
        alertRuleService.createRule(alertRuleDto);
        return ResponseResult.success();
    }

    @GetMapping("/read/{id}")
    @ApiOperation(value = "获取单个告警规则")
    public ResponseResult<AlertRuleVo> readAlertRule(@PathVariable Long id) {
        AlertRuleVo vo = alertRuleService.readAlertRule(id);
        return ResponseResult.success(vo);
    }

    @PutMapping("/update/{id}")
    @ApiOperation(value = "更新告警规则")
    public ResponseResult<Void> updateAlertRule(@PathVariable Long id,
                                                @RequestBody AlertRuleDto alertRuleDto) {
        alertRuleService.updateAlertRule(id, alertRuleDto);
        return ResponseResult.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除告警规则")
    public ResponseResult<Void> deleteAlertRule(@PathVariable Long id) {
        alertRuleService.deleteAlertRule(id);
        return ResponseResult.success();
    }

    @GetMapping("/get-page")
    @ApiOperation(value = "分页获取告警规则列表")
    public ResponseResult<PageBean<AlertRuleVo>> getAlertRulePage(AlertRuleQueryDto dto) {
        PageBean<AlertRuleVo> pageBean = alertRuleService.getAlertRulePage(dto);
        return ResponseResult.success(pageBean);
    }


    @PutMapping("/{id}/status/{status}")
    @ApiOperation(value = "启用/禁用")
    public ResponseResult<Void> enableOrDisable(@PathVariable Long id,
                                                @PathVariable Integer status) {
        alertRuleService.enableOrDisable(id, status);
        return ResponseResult.success();
    }
}
