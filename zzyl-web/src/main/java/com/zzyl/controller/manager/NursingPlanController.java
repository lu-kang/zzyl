package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.NursingPlanDto;
import com.zzyl.dto.NursingPlanPageQueryDto;
import com.zzyl.service.NursingPlanService;
import com.zzyl.vo.NursingPlanVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/nursing/plan")
@Api(tags = "护理计划管理")
@Slf4j
public class NursingPlanController {

    @Autowired
    NursingPlanService nursingPlanService;

    @ApiOperation("根据名称和状态分页查询")
    @GetMapping("/search")
    public ResponseResult<PageBean<NursingPlanVo>> pageQuery(NursingPlanPageQueryDto dto) {
        log.info("根据名称和状态分页查询: {}", dto);
        PageBean<NursingPlanVo> pageBean = nursingPlanService.pageQuery(dto);
        return ResponseResult.success(pageBean);
    }


    @ApiOperation("添加护理计划")
    @PostMapping
    public ResponseResult<Void> addNursingPlan(@RequestBody NursingPlanDto dto) {
        log.info("添加护理计划:{}", dto);
        nursingPlanService.addNursingPlan(dto);
        return ResponseResult.success();
    }


    @ApiOperation("根据ID查询护理计划")
    @GetMapping("/{id}")
    public ResponseResult<NursingPlanVo> getDetailById(@PathVariable Long id) {
        log.info("根据ID查询护理计划:{}", id);
        NursingPlanVo nursingPlanVo = nursingPlanService.getDetailById(id);
        return ResponseResult.success(nursingPlanVo);
    }

    @ApiOperation("修改护理计划")
    @PutMapping("/{id}")
    public ResponseResult<Void> updateNursingPlan(@PathVariable Long id,
                                                  @RequestBody NursingPlanDto dto) {
        log.info("修改护理计划:{}", dto);
        dto.setId(id);
        nursingPlanService.updateNursingPlan(dto);
        return ResponseResult.success();
    }

    @ApiOperation("删除护理计划")
    @DeleteMapping("/{id}")
    public ResponseResult<Void> deleteById(@PathVariable Long id) {
        log.info("删除护理计划:{}", id);
        nursingPlanService.deleteById(id);
        return ResponseResult.success();
    }

    @ApiOperation("启用、禁用护理计划")
    @PutMapping("/{id}/status/{status}")
    public ResponseResult<Void> enableOrDisable(@PathVariable Long id,
                                                @PathVariable Integer status) {
        log.info("启用、禁用护理计划:{} {}", id, status);
        nursingPlanService.enableOrDisable(id, status);
        return ResponseResult.success();
    }

    @ApiOperation("查询所有护理计划")
    @GetMapping
    public ResponseResult<List<NursingPlanVo>> getAll() {
        log.info("查询所有护理计划");
        List<NursingPlanVo> nursingPlanVoList = nursingPlanService.getAll();
        return ResponseResult.success(nursingPlanVoList);
    }
}
