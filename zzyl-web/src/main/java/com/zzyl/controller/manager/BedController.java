package com.zzyl.controller.manager;

import com.zzyl.base.ResponseResult;
import com.zzyl.dto.BedDto;
import com.zzyl.service.BedService;
import com.zzyl.vo.BedVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bed")
@Api(tags = "床位管理")
@Slf4j
public class BedController {

    @Autowired
    BedService bedService;


    @PostMapping("/create")
    @ApiOperation("新增床位")
    public ResponseResult<Void> createBed(@RequestBody BedDto dto) {
        log.info("新增床位:{}", dto);
        bedService.createBed(dto);
        return ResponseResult.success();
    }


    @ApiOperation("根据ID查询床位")
    @GetMapping("read/{id}")
    public ResponseResult<BedVo> getById(@PathVariable Long id) {
        log.info("根据ID查询床位:{}", id);
        BedVo bedVo = bedService.getById(id);
        return ResponseResult.success(bedVo);
    }


    @ApiOperation("更新床位")
    @PutMapping("/update")
    public ResponseResult<Void> updateBed(@RequestBody BedDto dto) {
        log.info("更新床位:{}", dto);
        bedService.updateBed(dto);
        return ResponseResult.success();
    }


    @ApiOperation("删除床位")
    @DeleteMapping("delete/{id}")
    public ResponseResult<Void> deleteById(@PathVariable Long id) {
        log.info("删除床位:{}", id);
        bedService.deleteById(id);
        return ResponseResult.success();
    }
}
