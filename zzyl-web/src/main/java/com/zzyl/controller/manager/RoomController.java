package com.zzyl.controller.manager;

import com.zzyl.base.ResponseResult;
import com.zzyl.dto.RoomDto;
import com.zzyl.service.RoomService;
import com.zzyl.vo.RoomVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/room")
@Api(tags = "房间管理")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @ApiOperation("获取所有房间（智能床位）")
    @GetMapping("/getRoomsWithDeviceByFloorId/{floorId}")
    public ResponseResult<List<RoomVo>> getRoomsWithDeviceByFloorId(@PathVariable Long floorId) {
        log.info("获取所有房间（智能床位）:{}", floorId);
        List<RoomVo> roomVoList = roomService.getRoomsWithDeviceByFloorId(floorId);
        return ResponseResult.success(roomVoList);
    }

    ////////////////////////////////////////////////////////////////////////

    @PostMapping("/add")
    @ApiOperation("添加房间")
    public ResponseResult<Void> addRoom(@RequestBody RoomDto roomDto) {
        roomService.addRoom(roomDto);
        return ResponseResult.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除房间")
    public ResponseResult<Void> deleteRoom(@PathVariable Long id) {
        roomService.deleteRoom(id);
        return ResponseResult.success();
    }

    @PutMapping("/update")
    @ApiOperation("更新房间")
    public ResponseResult<Void> updateRoom(@RequestBody RoomDto roomDto) {
        roomService.updateRoom(roomDto);
        return ResponseResult.success();
    }

    @GetMapping("/get/{id}")
    @ApiOperation("获取房间")
    public ResponseResult<RoomVo> getRoom(@PathVariable Long id) {
        RoomVo roomVo = roomService.getRoom(id);
        return ResponseResult.success(roomVo);
    }

    @GetMapping("/getAllVo")
    @ApiOperation("获取所有房间（所有楼层）")
    public ResponseResult<List<RoomVo>> getAllRoomVos() {
        List<RoomVo> roomVos = roomService.getAllRoomVos();
        return ResponseResult.success(roomVos);
    }

    @GetMapping("/getRoomsByFloorId/{floorId}")
    @ApiOperation("获取所有房间-入住配置")
    public ResponseResult<List<RoomVo>> getRoomsByFloorId(@PathVariable Long floorId) {
        List<RoomVo> roomVos = roomService.getRoomsByFloorId(floorId);
        return ResponseResult.success(roomVos);

    }


    @GetMapping("/getRoomsCheckedByFloorId/{floorId}")
    @ApiOperation("获取所有房间-床位房型")
    public ResponseResult<List<RoomVo>> getRoomsCheckedByFloorId(@PathVariable Long floorId) {
        List<RoomVo> roomVos = roomService.getRoomsCheckedByFloorId(floorId);
        return ResponseResult.success(roomVos);
    }

    @GetMapping("/getRoomsWithNurByFloorId/{floorId}")
    @ApiOperation("获取所有房间-负责老人")
    public ResponseResult<List<RoomVo>> getRoomsWithNurByFloorId(@PathVariable Long floorId) {
        List<RoomVo> roomVos = roomService.getRoomsWithNurByFloorId(floorId);
        return ResponseResult.success(roomVos);
    }

}
