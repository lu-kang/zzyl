package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.NursingLevelDto;
import com.zzyl.dto.NursingLevelPageQueryDto;
import com.zzyl.service.NursingLevelService;
import com.zzyl.vo.NursingLevelVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/nursingLevel")
@Api(tags = "护理等级管理")
@Slf4j
@RequiredArgsConstructor
public class NursingLevelController {

    final NursingLevelService nursingLevelService;

    @GetMapping("/listByPage")
    @ApiOperation("分页查询护理等级信息")
    public ResponseResult<PageBean<NursingLevelVo>> pageQuery(NursingLevelPageQueryDto dto) {
        log.info("分页查询护理等级信息:{}", dto);
        PageBean<NursingLevelVo> pageBean = nursingLevelService.pageQuery(dto);
        return ResponseResult.success(pageBean);
    }

    @PostMapping("/insert")
    @ApiOperation("插入护理等级信息")
    public ResponseResult<Void> insert(@RequestBody NursingLevelDto dto) {
        log.info("插入护理等级信息:{}", dto);
        nursingLevelService.addNursingLevel(dto);
        return ResponseResult.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("根据ID查询护理等级信息")
    public ResponseResult<NursingLevelVo> findById(@PathVariable("id") Long id) {
        log.info("根据ID查询护理等级信息:{}", id);
        NursingLevelVo nursingLevelVo = nursingLevelService.getDetailById(id);
        return ResponseResult.success(nursingLevelVo);
    }


    @PutMapping("/update")
    @ApiOperation("更新护理等级信息")
    public ResponseResult<Void> updateNursingLevel(@RequestBody NursingLevelDto dto) {
        log.info("更新护理等级信息:{}", dto);
        nursingLevelService.updateNursingLevel(dto);
        return ResponseResult.success();
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除护理等级信息")
    public ResponseResult<Void> delete(@PathVariable("id") Long id) {
        log.info("删除护理等级信息:{}", id);
        nursingLevelService.deleteById(id);
        return ResponseResult.success();
    }


    @PutMapping("/{id}/status/{status}")
    @ApiOperation("启用、禁用护理等级")
    public ResponseResult<Void> enableOrDisable(@PathVariable Long id,
                                                @PathVariable Integer status) {
        log.info("启用、禁用护理等级:{} {}", id, status);
        nursingLevelService.enableOrDisable(id, status);
        return ResponseResult.success();
    }

    @GetMapping("/listAll")
    @ApiOperation("查询所有护理等级信息")
    public ResponseResult<List<NursingLevelVo>> getAll() {
        log.info("查询所有护理等级信息");
        List<NursingLevelVo> list = nursingLevelService.getAll();
        return ResponseResult.success(list);
    }
}