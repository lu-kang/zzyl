
package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.AlertDataHandleDto;
import com.zzyl.dto.AlertDataPageQueryDto;
import com.zzyl.service.AlertDataService;
import com.zzyl.vo.AlertDataVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/alert-data")
@Api(tags = "报警数据相关接口")
@RequiredArgsConstructor
public class AlertDataController {

    final AlertDataService alertDataService;

    @ApiOperation(value = "分页查询报警数据")
    @GetMapping("/pageQuery")
    public ResponseResult<PageBean<AlertDataVo>> pageQuery(AlertDataPageQueryDto dto) {
        log.info("分页查询报警数据:{}", dto);
        PageBean<AlertDataVo> pageBean = alertDataService.pageQuery(dto);
        return ResponseResult.success(pageBean);
    }

    @ApiOperation("处理设备报警数据")
    @PutMapping("/handleAlertData/{id}")
    public ResponseResult<Void> handleAlertData(@PathVariable Long id,
                                                @RequestBody AlertDataHandleDto dto) {
        log.info("处理设备报警数据:{}", dto);
        alertDataService.handleAlertData(id, dto);
        return ResponseResult.success();
    }
}