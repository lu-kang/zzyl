package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.service.RetreatService;
import com.zzyl.vo.RetreatDetailVo;
import com.zzyl.vo.RetreatPageQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/retreat")
@Api(tags = "退住管理相关接口")
public class RetreatController {
    @Autowired
    private RetreatService retreatService;

    @ApiOperation(value = "分页查询")
    @GetMapping("/pageQuery")
    public ResponseResult<PageBean<RetreatPageQueryVo>> pageQuery(@RequestParam(value = "elderName", required = false) String elderName,
                                                                  @RequestParam(value = "elderIdCardNo", required = false) String elderIdCardNo,
                                                                  @RequestParam(value = "retreatStartTime", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime retreatStartTime,
                                                                  @RequestParam(value = "retreatEndTime", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime retreatEndTime,
                                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        return ResponseResult.success(retreatService.pageQuery(elderName, elderIdCardNo, retreatStartTime, retreatEndTime, pageNum, pageSize));
    }

    @GetMapping("/detail/{id}")
    @ApiOperation(value = "查询退住详情")
    public ResponseResult<RetreatDetailVo> detail(@PathVariable("id") Long id) {
        return ResponseResult.success(retreatService.detail(id));
    }
}
