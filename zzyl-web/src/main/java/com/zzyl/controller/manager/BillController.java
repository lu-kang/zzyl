package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.BillPageQueryDto;
import com.zzyl.service.BalanceService;
import com.zzyl.service.BillService;
import com.zzyl.vo.BalanceVo;
import com.zzyl.vo.BillVo;
import com.zzyl.vo.PrepaidRechargeRecordVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "账单管理")
@RestController
@RequestMapping("/bill")
public class BillController {

    @Autowired
    private BillService billService;
    @Autowired
    private BalanceService balanceService;

    @ApiOperation("分页查询账单")
    @GetMapping("/page")
    public ResponseResult<PageBean<BillVo>> pageBill(BillPageQueryDto dto) {
        PageBean<BillVo> pageBean = billService.pageQuery4Admin(dto);
        return ResponseResult.success(pageBean);
    }

    @ApiOperation("根据id查询账单")
    @GetMapping("/{id}")
    public ResponseResult<BillVo> getById(@PathVariable Long id) {
        BillVo billVo = billService.selectByPrimaryKey(id);
        return ResponseResult.success(billVo);
    }

    @ApiOperation("分页查询欠费账单")
    @GetMapping("/arrears/")
    public ResponseResult<PageBean<BillVo>> arrears(@RequestParam(name = "bedNo", required = false) String bedNo,
                                                    @RequestParam(name = "elderName", required = false) String elderName,
                                                    @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        PageBean<BillVo> arrears = billService.arrears(bedNo, elderName, pageNum, pageSize);
        return ResponseResult.success(arrears);
    }

    @ApiOperation("分页查询余额")
    @GetMapping("/balance/")
    public ResponseResult<PageBean<BalanceVo>> balance(@RequestParam(name = "bedNo", required = false) String bedNo,
                                                       @RequestParam(name = "elderName", required = false) String elderName,
                                                       @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                                       @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        PageBean<BalanceVo> pageBean = balanceService.page(bedNo, elderName, pageNum, pageSize);
        return ResponseResult.success(pageBean);
    }


    @ApiOperation("分页查询预付费充值记录")
    @GetMapping("/prepaidRechargeRecord/page")
    public ResponseResult<PageBean<PrepaidRechargeRecordVo>> prepaidRechargeRecordPage(@RequestParam(name = "bedNo", required = false) String bedNo,
                                                                                       @RequestParam(name = "elderName", required = false) String elderName,
                                                                                       @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                                                                       @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        PageBean<PrepaidRechargeRecordVo> pageBean = billService.prepaidRechargeRecordPage(bedNo, elderName, pageNum, pageSize);
        return ResponseResult.success(pageBean);
    }
}
