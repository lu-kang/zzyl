package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.ReservationQueryDto;
import com.zzyl.service.ReservationService;
import com.zzyl.vo.ReservationVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/reservation")
@Api(tags = "预约管理相关接口")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @GetMapping("/page")
    @ApiOperation("分页查询预约")
    public ResponseResult<PageBean<ReservationVo>> findByPage(ReservationQueryDto dto) {
        PageBean<ReservationVo> byPage = reservationService.findByPage4Admin(dto);
        return ResponseResult.success(byPage);
    }


}
