package com.zzyl.controller.manager;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.DeviceDataPageReqDto;
import com.zzyl.service.DeviceDataService;
import com.zzyl.vo.DeviceDataVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@Api(tags = "Iot设备数据管理")
@RequestMapping("/device-data")
public class DeviceDataController {

    @Autowired
    private DeviceDataService deviceDataService;

    @ApiOperation("分页查询设备中的数据")
    @GetMapping("/get-page")
    public ResponseResult<PageBean<DeviceDataVo>> pageQueryDeviceData(DeviceDataPageReqDto dto) {
        log.info("分页查询设备中的数据:{}", dto);
        PageBean<DeviceDataVo> pageBean = deviceDataService.pageQueryDeviceData(dto);
        return ResponseResult.success(pageBean);
    }
}
