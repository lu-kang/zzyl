package com.zzyl.controller.customer;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.NursingProjectPageQueryDto;
import com.zzyl.dto.OrderPageQueryDto;
import com.zzyl.service.NursingProjectService;
import com.zzyl.service.OrderService;
import com.zzyl.vo.NursingProjectVo;
import com.zzyl.vo.OrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "客户订单管理")
@RestController
@RequestMapping("/customer/orders")
@Slf4j
public class CustomOrderController {

    @Autowired
    private NursingProjectService nursingProjectService;
    @Autowired
    private OrderService orderService;

    @ApiOperation("分页查询护理项目列表")
    @GetMapping("/project/page")
    public ResponseResult<PageBean<NursingProjectVo>> pageQuery(NursingProjectPageQueryDto dto) {
        PageBean<NursingProjectVo> nursingProjectPageInfo = nursingProjectService.pageQuery(dto);
        return ResponseResult.success(nursingProjectPageInfo);
    }

    @ApiOperation("根据编号查询护理项目信息")
    @GetMapping("/project/{id}")
    public ResponseResult<NursingProjectVo> getById(@PathVariable Long id) {
        NursingProjectVo nursingProjectVO = nursingProjectService.getById(id);
        return ResponseResult.success(nursingProjectVO);
    }

    ////////////////////////////////////////////////// 课外任务

    @ApiOperation("分页查询订单")
    @GetMapping("/order/page")
    public ResponseResult<PageBean<OrderVo>> pageOrder(OrderPageQueryDto dto) {
        PageBean<OrderVo> pageBean = orderService.pageQuery4Customer(dto);
        return ResponseResult.success(pageBean);
    }

    @ApiOperation("根据id查询")
    @GetMapping("/{orderId}")
    public ResponseResult<OrderVo> getOrderById(@PathVariable("orderId") Long orderId) {
        OrderVo orderVo = orderService.getOrderById(orderId);
        return ResponseResult.success(orderVo);
    }
}




