package com.zzyl.controller.customer;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.ReservationDto;
import com.zzyl.dto.ReservationQueryDto;
import com.zzyl.service.ReservationService;
import com.zzyl.vo.ReservationVo;
import com.zzyl.vo.TimeCountVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/customer/reservation")
@Api(tags = "客户预约管理")
@RequiredArgsConstructor
public class CustomerReservationController {

    final ReservationService reservationService;

    @ApiOperation("查询取消预约数量")
    @GetMapping("/cancelled-count")
    public ResponseResult<Integer> cancelledCount() {
        log.info("查询取消预约数量");
        Integer count = reservationService.cancelledCount();
        return ResponseResult.success(count);
    }

    @ApiOperation("查询每个时间段剩余预约次数")
    @GetMapping("/countByTime")
    public ResponseResult<List<TimeCountVo>> countByTime(Long time) {
        log.info("查询每个时间段剩余预约次数:{}", time);
        List<TimeCountVo> list = reservationService.countByTime(time);
        return ResponseResult.success(list);
    }


    @ApiOperation("新增预约")
    @PostMapping
    public ResponseResult<Void> addReservation(@RequestBody ReservationDto dto) {
        log.info("新增预约:{}", dto);
        reservationService.addReservation(dto);
        return ResponseResult.success();
    }

    @GetMapping("/page")
    @ApiOperation("分页查询预约")
    public ResponseResult<PageBean<ReservationVo>> findByPage(ReservationQueryDto dto) {
        log.info("分页查询预约:{}", dto);
        PageBean<ReservationVo> reservationVoList = reservationService.findByPage4Customer(dto);
        return ResponseResult.success(reservationVoList);
    }

    @PutMapping("/{id}/cancel")
    @ApiOperation("取消预约")
    public ResponseResult<Void> cancelledReservation(@PathVariable Long id) {
        log.info("取消预约:{}", id);
        reservationService.cancelReservation(id);
        return ResponseResult.success();
    }
}
