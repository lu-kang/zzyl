package com.zzyl.controller.customer;

import com.aliyun.iot20180120.models.QueryDevicePropertyStatusRequest;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.QueryDeviceDataDto;
import com.zzyl.dto.UserLoginRequestDto;
import com.zzyl.service.DeviceDataService;
import com.zzyl.service.DeviceService;
import com.zzyl.service.MemberService;
import com.zzyl.vo.DeviceDataGraphVo;
import com.zzyl.vo.DevicePropertyStatusVo;
import com.zzyl.vo.WxLoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = "客户管理")
@RestController
@RequestMapping("/customer/user")
public class CustomerUserController {

    @Autowired
    MemberService memberService;
    @Autowired
    DeviceService deviceService;
    @Autowired
    DeviceDataService deviceDataService;

    @ApiOperation("C端用户登录--微信登录")
    @PostMapping("/login")
    public ResponseResult<WxLoginVo> wxLogin(@RequestBody UserLoginRequestDto dto) {
        log.info("C端用户登录--微信登录:{}", dto);
        WxLoginVo wxLoginVo = memberService.wxLogin(dto);
        return ResponseResult.success(wxLoginVo);
    }

    // ================================================= 家属端数据报表展示

    @ApiOperation("查询指定设备的物模型运行状态")
    @PostMapping("/QueryDevicePropertyStatus")
    public ResponseResult<DevicePropertyStatusVo> queryDevicePropertyStatus(@RequestBody QueryDevicePropertyStatusRequest request) {
        log.info("查询指定设备的物模型运行状态:{}", request);
        DevicePropertyStatusVo devicePropertyStatusVo = deviceService.queryDevicePropertyStatus(request);
        return ResponseResult.success(devicePropertyStatusVo);
    }

    @ApiOperation("按日查询设备数据")
    @GetMapping("/queryDeviceDataListByDay")
    public ResponseResult<List<DeviceDataGraphVo>> queryDeviceDataListByDay(QueryDeviceDataDto dto) {
        log.info("按日查询设备数据:{}", dto);
        List<DeviceDataGraphVo> list = deviceDataService.queryDeviceDataListByDay(dto);
        return ResponseResult.success(list);
    }

    //TODO @ApiOperation("按周查询设备数据")
}
