package com.zzyl.controller.customer;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.BillPageQueryDto;
import com.zzyl.service.BillService;
import com.zzyl.vo.BillVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "客户账单管理")
@RestController
@RequestMapping("/customer/bill")
public class CustomerBillController {

    @Autowired
    private BillService billService;

    @ApiOperation("分页查询账单")
    @GetMapping("/page")
    public ResponseResult<PageBean<BillVo>> pageBill(BillPageQueryDto dto) {
        PageBean<BillVo> pageBean = billService.pageQuery4Customer(dto);
        return ResponseResult.success(pageBean);
    }

    @ApiOperation("根据id查询账单")
    @GetMapping("/{id}")
    public ResponseResult<BillVo> getById(@PathVariable Long id) {
        BillVo billVo = billService.selectByPrimaryKey(id);
        return ResponseResult.success(billVo);
    }
}














