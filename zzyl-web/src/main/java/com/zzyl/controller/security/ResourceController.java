package com.zzyl.controller.security;

import com.zzyl.base.ResponseResult;
import com.zzyl.dto.ResourceDto;
import com.zzyl.service.ResourceService;
import com.zzyl.vo.MenuVo;
import com.zzyl.vo.ResourceVo;
import com.zzyl.vo.TreeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/resource")
@Api(tags = "资源管理")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    @ApiOperation("资源列表")
    @PostMapping("/list")
    public ResponseResult<List<ResourceVo>> listResource(@RequestBody ResourceDto dto) {
        log.info("资源列表:{}", dto);
        List<ResourceVo> list = resourceService.listResource(dto);
        return ResponseResult.success(list);
    }

    @ApiOperation("资源树形")
    @PostMapping("/tree")
    public ResponseResult<TreeVo> resourceTreeVo() {
        log.info("资源树形结构");
        TreeVo treeVo = resourceService.resourceTreeVo();
        return ResponseResult.success(treeVo);
    }

    @ApiOperation("资源添加")
    @PutMapping
    public ResponseResult<Void> addResource(@RequestBody ResourceDto dto) {
        log.info("资源添加:{}", dto);
        resourceService.addResource(dto);
        return ResponseResult.success();
    }

    @PatchMapping
    @ApiOperation(value = "资源修改", notes = "资源修改")
    public ResponseResult<Void> updateResource(@RequestBody ResourceDto resourceDto) {
        log.info("资源修改:{}", resourceDto);
        resourceService.updateResource(resourceDto);
        return ResponseResult.success();
    }

    @ApiOperation(value = "启用禁用", notes = "启用禁用")
    @PostMapping("/enable")
    public ResponseResult<Void> isEnable(@RequestBody ResourceDto resourceDto) {
        log.info("启用禁用:{}", resourceDto);
        resourceService.isEnable(resourceDto);
        return ResponseResult.success();
    }

    @ApiOperation("删除菜单")
    @DeleteMapping("/{resourceNo}")
    public ResponseResult<Void> remove(@PathVariable("resourceNo") String resourceNo) {
        log.info("删除菜单:{}", resourceNo);
        resourceService.deleteByResourceNo(resourceNo);
        return ResponseResult.success();
    }

    ////////////////////////////////////////////////////////////////////////////////////

    @GetMapping("/menus")
    @ApiOperation("我的菜单")
    public ResponseResult<List<MenuVo>> getMyMenus() {
        log.info("我的菜单");
        List<MenuVo> menus = resourceService.getMyMenus();
        return ResponseResult.success(menus);
    }
}
