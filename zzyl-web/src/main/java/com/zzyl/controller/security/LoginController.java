package com.zzyl.controller.security;

import com.zzyl.base.ResponseResult;
import com.zzyl.dto.LoginDto;
import com.zzyl.service.LoginService;
import com.zzyl.vo.LoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/security")
@Api(tags = "后台登录")
@RequiredArgsConstructor
public class LoginController {

    final LoginService loginService;

    @ApiOperation("后台用户登录")
    @PostMapping("/login")
    public ResponseResult<LoginVo> login(@RequestBody LoginDto dto) {
        log.info("后台用户登录:{}", dto);
        /*LoginVo loginVo = new LoginVo();
        loginVo.setUserToken("xxxxxxxxxxxxxx");
        loginVo.setNickName("admin");
        loginVo.setRealName("超级管理员");*/

        LoginVo loginVo = loginService.login(dto);
        return ResponseResult.success(loginVo);
    }
}
