package com.zzyl.controller.security;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.RoleDto;
import com.zzyl.service.RoleService;
import com.zzyl.vo.RoleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/role")
@Api(tags = "角色管理")
@RequiredArgsConstructor
public class RoleController {

    final RoleService roleService;

    @ApiOperation("角色分页")
    @PostMapping("/page/{pageNum}/{pageSize}")
    public ResponseResult<PageBean<RoleVo>> pageQuery(@PathVariable Integer pageNum,
                                                      @PathVariable Integer pageSize,
                                                      @RequestBody RoleDto dto) {
        log.info("角色分页:{} {} {}", pageNum, pageSize, dto);
        PageBean<RoleVo> pageBean = roleService.pageQuery(pageNum, pageSize, dto);
        return ResponseResult.success(pageBean);
    }

    @ApiOperation("角色添加")
    @PutMapping
    public ResponseResult<Void> addRole(@RequestBody RoleDto dto) {
        log.info("角色添加:{}", dto);
        roleService.addRole(dto);
        return ResponseResult.success();
    }

    @ApiOperation("根据角色查询选中的资源数据")
    @GetMapping("/find-checked-resources/{roleId}")
    public ResponseResult<Set<String>> findCheckedResource(@PathVariable Long roleId) {
        log.info("根据角色查询选中的资源数据:{}", roleId);
        Set<String> list = roleService.findCheckedResource(roleId);
        return ResponseResult.success(list);
    }

    @ApiOperation("角色修改")
    @PatchMapping
    public ResponseResult<Void> updateRole(@RequestBody RoleDto dto) {
        log.info("角色修改:{}", dto);
        roleService.updateRole(dto);
        return ResponseResult.success();
    }

    @ApiOperation(value = "删除角色")
    @DeleteMapping("/{id}")
    public ResponseResult<Void> deleteById(@PathVariable Long id) {
        log.info("删除角色:{} ", id);
        roleService.deleteById(id);
        return ResponseResult.success();
    }

    @PostMapping("init-roles")
    @ApiOperation(value = "查询所有角色", notes = "角色下拉框")
    ResponseResult<List<RoleVo>> list() {
        log.info("查询所有角色");
        List<RoleVo> roleVoResult = roleService.list();
        return ResponseResult.success(roleVoResult);
    }
}
