package com.zzyl.controller.security;

import com.zzyl.base.PageBean;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.UserDto;
import com.zzyl.service.UserService;
import com.zzyl.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = "用户管理")
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    final UserService userService;

    @ApiOperation("用户分页")
    @PostMapping("/page/{pageNum}/{pageSize}")
    public ResponseResult<PageBean<UserVo>> pageQuery(@PathVariable Integer pageNum,
                                                      @PathVariable Integer pageSize,
                                                      @RequestBody UserDto dto) {
        log.info("用户分页:{} {} {}", pageNum, pageSize, dto);
        PageBean<UserVo> pageBean = userService.pageQuery(pageNum, pageSize, dto);
        return ResponseResult.success(pageBean);
    }

    @PutMapping
    @ApiOperation(value = "用户添加", notes = "用户添加")
    public ResponseResult<Void> createUser(@RequestBody UserDto userDto) {
        log.info("用户添加:{}", userDto);
        userService.createUser(userDto);
        return ResponseResult.success();
    }

    @PatchMapping
    @ApiOperation(value = "用户修改", notes = "用户修改")
    public ResponseResult<Void> updateUser(@RequestBody UserDto userDto) {
        log.info("用户修改:{}", userDto);
        userService.updateUser(userDto);
        return ResponseResult.success();
    }

    @ApiOperation("删除用户")
    @DeleteMapping("/remove/{userId}")
    public ResponseResult<Void> remove(@PathVariable Long userId) {
        log.info("删除用户:{}", userId);
        userService.deleteUserById(userId);
        return ResponseResult.success();
    }

    @PutMapping("/is-enable/{id}/{status}")
    @ApiOperation("启用或禁用用户")
    public ResponseResult<Void> isEnable(@PathVariable(name = "id") Long id,
                                         @PathVariable(name = "status") String status) {
        log.info("启用或禁用用户:{} {}", id, status);
        userService.isEnable(id, status);
        return ResponseResult.success();
    }

    @PostMapping("/list")
    @ApiOperation(value = "用户列表", notes = "用户列表")
    public ResponseResult<List<UserVo>> userList(@RequestBody UserDto userDto) {
        log.info("用户列表:{}", userDto);
        List<UserVo> userVoList = userService.findUserList(userDto);
        return ResponseResult.success(userVoList);
    }

    @PostMapping("/reset-passwords/{userId}")
    @ApiOperation(value = "密码重置", notes = "密码重置")
    public ResponseResult<Void> resetPasswords(@PathVariable("userId") Long userId) {
        log.info("密码重置:{}", userId);
        userService.resetPasswords(userId);
        return ResponseResult.success();
    }

    @GetMapping("/current-user")
    @ApiOperation(value = "个人信息", notes = "个人信息")
    public ResponseResult<UserVo> getCurrentUser() {
        log.info("个人信息");
        UserVo userVo = userService.getCurrentUser();
        return ResponseResult.success(userVo);
    }
}
