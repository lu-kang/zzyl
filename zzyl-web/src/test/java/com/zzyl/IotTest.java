package com.zzyl;

import com.aliyun.iot20180120.Client;
import com.aliyun.iot20180120.models.QueryProductListRequest;
import com.aliyun.iot20180120.models.QueryProductListResponse;
import com.zzyl.properties.AliyunIotProperties;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class IotTest {

    @Autowired
    Client client;
    @Autowired
    AliyunIotProperties aliyunIotProperties;

    /**
     * 查询产品列表
     */
    @Test
    public void testSelectProductList() throws Exception {

        //1. 创建请求参数对象
        QueryProductListRequest request = new QueryProductListRequest();

        request.setCurrentPage(1);
        request.setPageSize(100);
        request.setIotInstanceId(aliyunIotProperties.getIotInstanceId()); //实例id必须设置

        //2. 通过Client对象，发送Http请求
        QueryProductListResponse response = client.queryProductList(request);


        //3. 解析响应数据
        if (!response.body.success) {
            System.out.println(response.body.errorMessage);
            return;
        }

        response.body.data.list.productInfo.forEach(p -> {
            System.out.println(p.productKey);
            System.out.println(p.productName);
        });

    }
}
