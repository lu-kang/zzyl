package com.zzyl;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class HttpTest {

    /**
     * get和post请求
     */
    @Test
    public void testGet() {
    }

    @Test
    public void testGetProjectList() {
        String url = "http://localhost:9995/nursing_project";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("pageNum", 1);
        paramMap.put("pageSize", 10);
    }

    @Test
    public void testPost() {
        String url = "http://localhost:9995/nursing_project";
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("name", "护理项目测试3211违反");
        paramMap.put("orderNo", 1);
        paramMap.put("unit", "次");
        paramMap.put("price", 10.00);
        paramMap.put("image", "https://yjy-slwl-oss.oss-cn-hangzhou.aliyuncs.com/ae7cf766-fb7b-49ff-a73c-c86c25f280e1.png");
        paramMap.put("nursingRequirement", "无特殊要求");
        paramMap.put("status", 1);

        //通过Hutool发送请求
        //没有指定请求体的数据格式
        HttpResponse response = HttpUtil.createPost(url)
                .header("Content-Type", "application/json")
                .body(JSONUtil.toJsonStr(paramMap))
                .execute();
        System.out.println(response.body());
    }


    /**
     * 根据快递单号查询物流信息
     */
    @Test
    public void testAliApi1() {
        //发送http请求，接口文档的四要素

        //请求路径
        String url = "https://wuliu.market.alicloudapi.com/kdi";
        //请求方式：GET
        //请求参数：no=1234567890  type=ZTO1234567890
        //请求头：Authorization:APPCODE fb0a9785862a4eff81a28df23cd29d1c
        //响应数据：

        //通过hutool工具包发送http请求
        HttpResponse response = HttpUtil.createGet(url)
                //设置请求头
                .header("Authorization", "APPCODE fb0a9785862a4eff81a28df23cd29d1c")
                //设置请求参数
                .form("no", "JD0145159666995")
                //发送请求
                .execute();

        System.out.println(response.body());
    }

    /**
     * 根据城市信息查询天气情况
     */
    @Test
    public void testAliApi2() {
        //发送http请求，接口文档的四要素

        //请求路径
        String url = "https://ali-weather.showapi.com/spot-to-weather";

        //通过hutool工具包发送http请求
        HttpResponse response = HttpUtil.createGet(url)
                //设置请求头
                .header("Authorization", "APPCODE fb0a9785862a4eff81a28df23cd29d1c")
                //设置请求参数
                .form("area", "武汉")
                //发送请求
                .execute();

        System.out.println(response.body());
    }


}
