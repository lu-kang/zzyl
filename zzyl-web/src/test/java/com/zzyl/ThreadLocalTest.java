package com.zzyl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadLocalTest {

    public static void main(String[] args) {

        //创建一个本地线程对象（存数据）
        ThreadLocal<Integer> tl = new ThreadLocal<>();

        tl.set(1); //往主线程存放数据

        new Thread(new Runnable() {
            @Override
            public void run() {
                //tl.set(2); //往子线程A存放数据
                System.out.println("子线程A");
                System.out.println(tl.get());//null
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                tl.set(3); //往子线程B存放数据
                System.out.println("子线程B");
            }
        }).start();

        System.out.println("主线程");
    }
}