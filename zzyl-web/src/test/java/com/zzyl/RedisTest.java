package com.zzyl;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;

@SpringBootTest
public class RedisTest {

    @Resource
    StringRedisTemplate stringRedisTemplate;

    @Test
    public void test() {
        //存入数据
        stringRedisTemplate.opsForValue().set("username", "张三");

        //取数据
        String username = stringRedisTemplate.opsForValue().get("username");
        System.out.println(username);
    }
}
