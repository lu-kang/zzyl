package com.zzyl;

import cn.hutool.crypto.digest.DigestUtil;

public class PwdTest {

    public static void main(String[] args) {
        //md5加密
        System.out.println(DigestUtil.md5Hex("123456"));
        System.out.println(DigestUtil.md5Hex("123456"));
        System.out.println(DigestUtil.md5Hex("123456"));

        //BCrypt加密
        for (int i = 0; i < 20; i++) {
            System.out.println(DigestUtil.bcrypt("888itcast.CN764%..."));
        }

        //$2a$10$h5eFCfpHbCApqXFFI4o7zeSi3MHrlPnmKfoe6YvhipprhSiFwmbzi
        //$2a$10$VB9D5iGoQHisdUV104nTFeDwP8Tw6786SBnGW/vGgO117e3wOa8AW
        //$2a$10$gzMdZ5sPqdoLvbUYY13YKufYfVmD8wmQVaxqCO1XJgtQd1WoLKDXm

        System.out.println(DigestUtil.bcryptCheck("123456", "$2a$10$h5eFCfpHbCApqXFFI4o7zeSi3MHrlPnmKfoe6YvhipprhSiFwmbzi"));
        System.out.println(DigestUtil.bcryptCheck("123456", "$2a$10$VB9D5iGoQHisdUV104nTFeDwP8Tw6786SBnGW/vGgO117e3wOa8AW"));
        System.out.println(DigestUtil.bcryptCheck("123456", "$2a$10$gzMdZ5sPqdoLvbUYY13YKufYfVmD8wmQVaxqCO1XJgtQd1WoLKDXm"));
        System.out.println(DigestUtil.bcryptCheck("123456", "$2a$10$gzMdZ5sPqdoLvbUYY13YKufYfVmD8wmQVaxqCO1XJgtQd1WoLKDXm"));

        System.out.println(DigestUtil.bcryptCheck("888itcast.CN764%...", "$2a$10$4xM9PrGnVVKcMyIMjxYurO/G6kDwg8CZxUiVd1EB0GdPqph5UAs1i"));
    }
}
